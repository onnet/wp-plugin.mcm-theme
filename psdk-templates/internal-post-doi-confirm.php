<?php if (!get_query_var('service_theme_banner_id')) { ?>

    <div class="mcm-row">
        <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>
        <?php if ('' !== get_query_var('msisdn')): ?>
            <h2><?php echo apply_filters('get_template_copy', 'text'); ?></h2>
        <?php endif; ?>
    </div>
    <div class="mcm-row">
        <div class="mcm-cta-unit">
            <a href="<?php echo get_service_url(false, 'process_internal_doi'); ?>" class="mcm-button"><?php echo apply_filters('get_template_copy', 'doi_submit_button_text'); ?></a>
        </div>
        <a href="<?php echo site_url(); ?>" class="mcm-button-neg"><?php echo apply_filters('get_template_copy', 'doi_cancel_button_text'); ?></a>
    </div>

<?php } else { ?>
    <!-- APPLIED SERVICE LEVEL THEME -->
    <div class="mcm-row">
        <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>
        <h2><?php echo apply_filters('get_template_copy', 'text'); ?></h2>
    </div>
    <div class="mcm-row">
        <div class="mcm-cta-unit">
            <a href="<?php echo get_service_url(false, 'process_internal_doi'); ?>" class="mcm-button theme-large-text"><?php echo apply_filters('get_template_copy', 'doi_submit_button_text'); ?></a>
        </div>
        <a href="<?php echo site_url(); ?>" class="mcm-button-neg"><?php echo apply_filters('get_template_copy', 'doi_cancel_button_text'); ?></a>
    </div>

<?php } ?>
