<?php // TODO: refactor this code so we rather call a hook to get any template notifications ?>

<?php if (isset($_GET['template_notification'])) { ?>
    <div class="mcm-info-bar">
        <span>Agree to the terms and conditions below.</span>
    </div>
<?php } ?>

<?php if (isset($_GET['similar_service'])) { ?>
    <div class="mcm-info-bar">
        <span>Hey, looks like you're already subscribed to that service. Here is a similar service.</span>
    </div>
<?php } ?>
