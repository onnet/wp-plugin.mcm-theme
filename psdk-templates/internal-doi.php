<?php if (!get_query_var('service_theme_banner_id')) { ?>
    <div class="mcm-row">
        <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>
        <h2><?php echo apply_filters('get_template_copy', 'text'); ?></h2>
    </div>


    <div class="mcm-row">
        <?php if (get_option('psdk_use_internal_pre_doi') == 'true'): ?>
            <div class="mcm-cta-unit">
                <a href="<?php echo psdk_parse_url(get_the_permalink(), ['pid' => '1.2'], true, ['pid']); ?>" class="mcm-button"><?php _e('Yes', 'portal-sdk'); ?></a>
            </div>
        <?php else: ?>
            <div class="mcm-cta-unit">
                <a href="<?php echo get_service_url(false, 'process_internal_doi'); ?>" class="mcm-button"><?php echo apply_filters('get_template_copy', 'doi_submit_button_text'); ?></a>
            </div>
        <?php endif; ?>
        <a href="<?php echo site_url(); ?>" class="mcm-button-neg"><?php echo apply_filters('get_template_copy', 'doi_cancel_button_text'); ?></a>
    </div>

<?php } else { ?>
    <!-- APPLIED SERVICE LEVEL THEME -->
    <div class="mcm-row">
        <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>
    </div>
    <div class="mcm-row">
        <?php if (get_option('psdk_use_internal_pre_doi') == 'true'): ?>
            <div class="mcm-cta-unit">
                <a href="<?php echo psdk_parse_url(get_the_permalink(), ['pid' => '1.2'], true, ['pid']); ?>" class="mcm-button"><?php _e('Yes', 'portal-sdk'); ?></a>
            </div>
        <?php else: ?>
            <div class="mcm-cta-unit">
                <a href="<?php echo get_service_url(false, 'process_internal_doi'); ?>" class="mcm-button theme-large-text"><?php echo apply_filters('get_template_copy', 'doi_submit_button_text'); ?></a>
            </div>
        <?php endif; ?>
        <h2><?php _e(the_title('', '', false), 'portal-sdk'); ?> costs <?php echo psdk_price_point(); ?></h2>
        <h2><?php echo apply_filters('get_template_copy', 'text'); ?></h2>
        <a href="<?php echo site_url(); ?>" class="mcm-button-neg"><?php echo apply_filters('get_template_copy', 'doi_cancel_button_text'); ?></a>
    </div>

<?php } ?>
