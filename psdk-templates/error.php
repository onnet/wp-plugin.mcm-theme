<div class="mcm-row">
    <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>
    <h2><?php echo apply_filters('get_template_copy', 'text'); ?></h2>
</div>

<div class="mcm-row">
    <?php psdk_include_template_part('button', 'retry'); ?>
    <?php psdk_include_template_part('content', 'cross-sell'); ?>
</div>