<div class="mcm-row">
<!--    --><?php //if (class_exists('class-mcm-theme')) { ?>
<!--        <h1>YOLO</h1>-->
<!--    --><?php //} ?>
<!--    <h1><h1>--><?php //_e('1Enter your number below to subscribe to ', 'portal-sdk'); ?><!----><?php //_e(the_title('', '', false), 'portal-sdk'); ?><!----><?php //_e(' for ', 'portal-sdk') ?><!----><?php //echo psdk_price_point('<span>', '</span>'); ?><!--</h1></h1>-->

    <h1><?php echo apply_filters('get_template_copy', 'heading'); ?></h1>

    <form method="GET" action="<?php echo the_permalink(); ?>" class="mcm-form">
        <div class="mcm-text-input">
            <?php wp_nonce_field('manual_msisdn', 'psdk_action', false); ?>
            <input type="<?php echo isLowTech() ? 'text' : 'tel'; ?>" class="text-input"
                   placeholder="<?php _e('Enter your number here...', 'portal-sdk'); ?>"
                   name="msisdn"
                   value="<?php echo apply_filters('psdk_friendly_msisdn', psdk_get_enriched_msisdn()); ?>">

            <input type="hidden" class="text-input"
                   name="cid"
                   value="<?php echo get_query_var('cid'); ?>">

            <input type="hidden" class="text-input"
                   name="access_token"
                   value="<?php echo get_query_var('access_token'); ?>">
        </div>

        <?php if (get_post_meta(get_the_ID(), 'facebook_ad_service', true)) { ?>
            <div class="mcm-checkbox">
                <input type="checkbox" name="agree_to_terms" id="agree_to_terms">
                <label for="agree_to_terms">I agree to the <a href="<?php echo get_post_meta(get_the_ID(), '_terms_conditions_url', true); ?>" target="_blank">terms and conditions</a>.</label>
            </div>
        <?php } ?>

        <div class="mcm-cta-unit">
            <button type="submit" value="Submit CTA" class="mcm-button">
                <span><?php _e('Get Started', 'portal-sdk'); ?></span>
            </button>
        </div>
    </form>
</div>
