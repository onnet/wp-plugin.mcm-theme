<div class="mcm-cta-unit <?php echo !psdk_cross_sell_enabled() ?: 'lowkey'; ?>">
    <a href="<?php echo home_url(); ?>" class="mcm-button" id="countdown-button"><?php echo apply_filters('get_template_copy', 'continue_button_text'); ?></a>
</div>