<?php if (psdk_cross_sell_enabled()): ?>
	<?php $relations = psdk_get_service_relations_filtered(); ?>
    <?php if (!empty($relations)): $countRelations = count($relations); ?>
        <div class="mcm-divider"></div>

        <h1><?php _e('You might also like this service', 'portal-sdk'); ?></h1>

        <?php if (is_array($relations)): for ($x = 0; $x < $countRelations; $x++): ?>

            <?php if ($x % 2 == 0): echo '<div class="mcm-row spacing">'; endif; ?>

            <div class="mcm-col-2 rounded">
                <a href="<?php echo the_cross_sell_url(get_the_permalink($relations[$x]['post_id'])); ?>">
                    <div class="mcm-img">
                        <?php echo apply_filters('get_image_html', $relations[$x]['attachment_id']); ?>
                        <div class="mcm-img-caption">
                            <span>
                                <?php echo $relations[$x]['copy'];?>
                            </span>
                        </div>
                    </div>
                </a>
            </div>

            <?php if ($x % 2 != 0): echo '</div>'; endif; ?>
        <?php
        endfor; endif;
        ?>

        <?php if ($countRelations % 2 != 0): echo '</div>'; endif; ?>
    <?php endif; ?>
<?php endif; ?>
