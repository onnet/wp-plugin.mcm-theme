<div class="mcm-cta-unit <?php echo !psdk_cross_sell_enabled() ?: 'lowkey'; ?>">
    <a href="<?php echo the_post_doi_url(); ?>" class="mcm-button" id="countdown-button"><?php echo psdk_filter_translation(__("Continue to ", 'portal-sdk') . __(get_the_title(), 'portal-sdk')); ?></a>
</div>