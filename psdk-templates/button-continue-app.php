<div class="mcm-cta-unit <?php echo !psdk_cross_sell_enabled() ?: 'lowkey'; ?>">
    <a href="<?php echo home_url(); ?>" class="mcm-button" id="countdown-button"><?php _e('Back to Home', 'portal-sdk'); ?></a>
</div>