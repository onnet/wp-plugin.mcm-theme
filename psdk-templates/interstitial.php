<div class="mainContent--variant1__confirmation">
    <div class="mainContent--variant1__innerWrapper">
        <div class="mainContent--variant1__head">
            <h1><?php _e('Subscribe to ', 'portal-sdk') ._e(get_the_title(), 'portal-sdk'). _e(' in just 2 easy steps!', 'portal-sdk'); ?></h1>
        </div>
        <div class="mainContent--variant1__body">
            <p><strong><?php _e('Step 1', 'portal-sdk'); ?></strong><br/>
                <?php _e('Click on the button below.', 'portal-sdk'); ?></p>

            <p><strong><?php _e('Step 2', 'portal-sdk'); ?></strong><br/>
                <?php _e('Reply with <strong>1: Yes</strong> to the confirmation message that will appear on your phone.', 'portal-sdk'); ?></p>
        </div>
        <div class="mainContent--variant1__ctaWrapper <?php echo !psdk_cross_sell_enabled() ?: 'lowkey'; ?>">
            <a href="<?php echo add_query_arg([
                'msisdn' => get_query_var('msisdn'),
                'pi'     => 'false',
            ], get_permalink()); ?>"><?php _e('Click here!', 'portal-sdk'); ?></a>
        </div>
    </div>
</div>