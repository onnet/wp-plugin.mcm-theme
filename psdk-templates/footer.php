    </div> <!-- End of .mcm-panel -->
</div> <!-- End of .mcm-panel .rounded -->


<div class="mcm-footer">
    <div class="mcm-row">
        <?php
        if(!(get_query_var('hide_logo_theme'))) { ?>
            <?php if(psdk_billing_channel_logo('footer', '<div class="footer-logo">', '</div>') !== false) { ?>
                <div class="mcm-col-2 twenty-five">
                    <div class="mcm-img">
                        <?php psdk_billing_channel_logo('footer', '<div class="footer-logo">', '</div>'); ?>
                    </div>
                </div>

                <div class="mcm-col-2 seventy-five">
                    <?php psdk_footer_content(); ?>
                </div>
            <?php } else { ?>
                <div class="mcm-col-2 seventy-five">
                    <?php psdk_footer_content(); ?>
                </div>
            <?php } ?>

        <?php } else { ?>
            <div class="mcm-col-2 seventy-five">
                <?php psdk_footer_content(); ?>
            </div>
        <?php }; ?>
    </div>
</div>