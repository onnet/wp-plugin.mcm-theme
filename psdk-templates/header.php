<?php psdk_billing_channel_logo('header', '<div class="contentWrapper--variant1__header">', '</div>'); ?>

<?php psdk_include_template_part('info-bar'); ?>

<div class="mcm-img">
    <?php if (has_post_thumbnail()): ?>
        <div class="mcm-img">
            <?php
            if (get_query_var('service_theme_banner_id')) {
                do_action('display_theme_banner');
            } else {
                the_post_thumbnail();
            } ?>
        </div>
    <?php endif; ?>
</div>

<div class="mcm-panel">
    <div class="mcm-panel rounded">
