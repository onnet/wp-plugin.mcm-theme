<div class="mcm-cta-unit">
    <a href="<?php the_try_again_url() ?>" class="mcm-button" id="countdown-button"><?php echo apply_filters('get_template_copy', 'try_again_button_text'); ?></a>
    <a href="<?php echo site_url('/d/' . get_query_var('subscription_id')); ?>" class="mcm-button-neg"><?php echo apply_filters('get_template_copy', 'replied_button_text'); ?></a>
</div>