<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    theme
 * @subpackage theme/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    theme
 * @subpackage theme/includes
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class theme_Activator
{
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        global $wpdb;

        // Templates
        $table_name = $wpdb->prefix . 'templates';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`                        INT(10)  NOT NULL AUTO_INCREMENT,
          `name`                      VARCHAR(50) NOT NULL,
          `slug`                      VARCHAR(50) NOT NULL,
          `template_key`              VARCHAR(10),
          `description`               VARCHAR(250),
          `created_at` DATETIME       NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);

        // Fields
        $table_name = $wpdb->prefix . 'fields';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`                  INT(10) NOT NULL AUTO_INCREMENT,
          `name`                VARCHAR(50) NOT NULL,
          `slug`                VARCHAR(50) NOT NULL,
          `description`         VARCHAR(250),
          `created_at`          DATETIME   NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);

        // Fields
        $table_name = $wpdb->prefix . 'template_fields';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`          INT(10)   NOT NULL AUTO_INCREMENT,
          `template_id` INT(10)   NOT NULL,
          `field_id`    INT(10)   NOT NULL,
          `created_at`  DATETIME  NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);


        // Populate Templates Table
        $created_at = date("Y-m-d H:i:s", time());
        $table = $wpdb->prefix . 'templates';

        $templates = [
            [
                'id'           => 1,
                'name'         => 'Wifi',
                'slug'         => 'wifi',
                'template_key' => '',
                'description'  => 'Default template shown when no msisdn is detected',
            ],
            [
                'id'           => 2,
                'name'         => 'DOI',
                'slug'         => 'doi',
                'template_key' => '1.1',
                'description'  => 'Internal DOI',
            ],
            [
                'id'           => 3,
                'name'         => 'Success Text',
                'slug'         => 'success-text',
                'template_key' => '2',
                'description'  => 'Successful Text Subscription',
            ],
            [
                'id'           => 4,
                'name'         => 'Success USSD',
                'slug'         => 'success-ussd',
                'template_key' => '2.1',
                'description'  => 'Successful USSD Subscription',
            ],
            [
                'id'           => 5,
                'name'         => 'Error',
                'slug'         => 'error',
                'template_key' => '3',
                'description'  => 'Default Error Template',
            ],
            [
                'id'           => 6,
                'name'         => 'Forbidden',
                'slug'         => 'forbidden',
                'template_key' => '3.1',
                'description'  => 'Forbidden on this network',
            ],
            [
                'id'           => 7,
                'name'         => 'Declined',
                'slug'         => 'declined',
                'template_key' => '3.2',
                'description'  => 'Declined subscription',
            ],
            [
                'id'           => 8,
                'name'         => 'Disable WiFi',
                'slug'         => 'disable-wifi',
                'template_key' => '3.3',
                'description'  => 'Disable WiFi template',
            ],
            [
                'id'           => 9,
                'name'         => 'Blocked',
                'slug'         => 'blocked',
                'template_key' => '3.4',
                'description'  => 'Device blocked template',
            ],
            [
                'id'           => 10,
                'name'         => 'Content Blocked',
                'slug'         => 'content-blocked',
                'template_key' => '3.5',
                'description'  => 'Content blocked template',
            ],
            [
                'id'           => 11,
                'name'         => 'Insufficient Funds',
                'slug'         => 'insufficient-funds',
                'template_key' => '3.6',
                'description'  => 'Insufficient Funds Template',
            ],
            [
                'id'           => 12,
                'name'         => 'Session Timeout',
                'slug'         => 'session-timeout',
                'template_key' => '3.7',
                'description'  => 'Session Timeout Template',
            ],
            [
                'id'           => 13,
                'name'         => 'Thank You',
                'slug'         => 'thank-you',
                'template_key' => '4',
                'description'  => 'Thank You for subscribing',
            ],
            [
                'id'           => 14,
                'name'         => 'Thank You Forced',
                'slug'         => 'thank-you-forced',
                'template_key' => '4.1',
                'description'  => 'Thank You for subscribing forced',
            ],
            [
                'id'           => 15,
                'name'         => 'Already Subscribed',
                'slug'         => 'text-subscribed',
                'template_key' => '4.2',
                'description'  => 'User is already subscribed',
            ],
            [
                'id'           => 16,
                'name'         => 'Duplicate Subscription',
                'slug'         => 'duplicate-request',
                'template_key' => '4.3',
                'description'  => 'SMS already sent to user',
            ],
            [
                'id'           => 17,
                'name'         => 'Thank You Cross-sell',
                'slug'         => 'thank-you-cross-sell',
                'template_key' => '4.4',
                'description'  => 'Cross sell template',
            ],
            [
                'id'           => 18,
                'name'         => 'Pending',
                'slug'         => 'pending',
                'template_key' => '4.5',
                'description'  => 'Users subscription is still pending',
            ],
            [
                'id'           => 19,
                'name'         => 'Already Subscribed Interstitial',
                'slug'         => 'text-subscribed-interstitial',
                'template_key' => '4.6',
                'description'  => 'User is already subscribed so try a different service',
            ],
            [
                'id'           => 20,
                'name'         => 'Forced Error',
                'slug'         => 'forced-error',
                'template_key' => '3.8',
                'description'  => 'Forced by CTA',
            ],
            [
                'id'           => 21,
                'name'         => 'Pre Internal Doi',
                'slug'         => 'pre-doi',
                'template_key' => '1.2',
                'description'  => 'Additional confirmation template shown before the internal doi template',
            ],
        ];

        foreach ($templates as $template) {
            $template['created_at'] = $created_at;
            $wpdb->insert($table, $template);
        }

        // Populate Fields Table
        $table = $wpdb->prefix . 'fields';

        $fields = [
            [
                'id'          => 1,
                'name'        => 'Heading',
                'slug'        => 'heading',
                'description' => 'The main heading text',
            ],
            [
                'id'          => 2,
                'name'        => 'Text',
                'slug'        => 'text',
                'description' => 'The smaller paragraph text',
            ],
            [
                'id'          => 3,
                'name'        => 'WiFI Submit Button Text',
                'slug'        => 'wifi_submit_button_text',
                'description' => 'The button text for the wifi submit button',
            ],
            [
                'id'          => 4,
                'name'        => 'Internal DOI Submit Button Text',
                'slug'        => 'doi_submit_button_text',
                'description' => 'The button to submit',
            ],
            [
                'id'          => 5,
                'name'        => 'Internal DOI Cancel Button Text',
                'slug'        => 'doi_cancel_button_text',
                'description' => 'The button to cancel',
            ],
            [
                'id'          => 6,
                'name'        => 'Try Again Button Text',
                'slug'        => 'try_again_button_text',
                'description' => 'The text for the try again button',
            ],
            [
                'id'          => 7,
                'name'        => 'I\'ve Replied Button Text',
                'slug'        => 'replied_button_text',
                'description' => 'The text for the I\'ve replied button',
            ],
            [
                'id'          => 8,
                'name'        => 'Refresh Button Text',
                'slug'        => 'refresh_button_text',
                'description' => 'The text for the refresh button',
            ],
            [
                'id'          => 9,
                'name'        => 'Continue Button Text',
                'slug'        => 'continue_button_text',
                'description' => 'The text for the continue button',
            ],
        ];

        foreach ($fields as $field) {
            $field['created_at'] = $created_at;
            $wpdb->insert($table, $field);
        }


        // Populate Template Fields Table
        $table = $wpdb->prefix . 'template_fields';

        $template_fields = [
            // WiFI Template
            [
                'template_id' => 1,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 1,
                'field_id'    => 3, // Wifi Submit Button Text
            ],

            // DOI Template (1.1)
            [
                'template_id' => 2,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 2,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 2,
                'field_id'    => 4, // DOI Submit Button Text
            ],
            [
                'template_id' => 2,
                'field_id'    => 5, // DOI Cancel Button Text
            ],

            // Success Text Template (2)
            [
                'template_id' => 3,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 3,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 3,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Success USSD Template (2.1)
            [
                'template_id' => 4,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 4,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 4,
                'field_id'    => 7, // I've replied Button Text
            ],
            [
                'template_id' => 4,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Error Template (3)
            [
                'template_id' => 5,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 5,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Forbidden Template (3.1)
            [
                'template_id' => 6,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 6,
                'field_id'    => 2, // Body Text
            ],

            // Declined Template (3.2)
            [
                'template_id' => 7,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 7,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 7,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Disable Wifi Template (3.3)
            [
                'template_id' => 8,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 8,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 8,
                'field_id'    => 8, // Refresh Button Text
            ],

            // Device Blocked Template (3.4)
            [
                'template_id' => 9,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 9,
                'field_id'    => 2, // Body Text
            ],

            // Content Blocked Template (3.5)
            [
                'template_id' => 10,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 10,
                'field_id'    => 2, // Body Text
            ],

            // Insufficient Airtime Template (3.6)
            [
                'template_id' => 11,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 11,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Session Timeout Template (3.7)
            [
                'template_id' => 12,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 12,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Thank You Template (4)
            [
                'template_id' => 13,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 13,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 13,
                'field_id'    => 9, // Continue Button Text
            ],

            // Thank You Forced Template (4.1)
            [
                'template_id' => 14,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 14,
                'field_id'    => 2, // Body Text
            ],

            // Already Subscribed Template (4.2)
            [
                'template_id' => 15,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 15,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 15,
                'field_id'    => 9, // Continue Button Text
            ],

            // Duplicate Request Template (4.3)
            [
                'template_id' => 16,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 16,
                'field_id'    => 2, // Body Text
            ],

            // Duplicate Request Template (4.4)
            [
                'template_id' => 17,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 17,
                'field_id'    => 9, // Continue Button Text
            ],

            // Pending Template (4.5)
            [
                'template_id' => 18,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 18,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 18,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Already Subscribed Interstitial Template (4.6)
            [
                'template_id' => 19,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 19,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 19,
                'field_id'    => 9, // Continue Button Text
            ],

            // FORCED Error Template (3.8)
            [
                'template_id' => 20,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 20,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Pre DOI Template (1.2)
            [
                'template_id' => 21,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 21,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 21,
                'field_id'    => 4, // DOI Submit Button Text
            ],
            [
                'template_id' => 21,
                'field_id'    => 5, // DOI Cancel Button Text
            ],
        ];

        foreach ($template_fields as $template_field) {
            $template_field['created_at'] = $created_at;
            $wpdb->insert($table, $template_field);
        }


        // Insert default copy
        $table = $wpdb->prefix . 'template_fields';
        $theme_post_id = null;

        // Check if a default/global theme already exists
        $theme_object = get_page_by_title('default', OBJECT, 'custom_theme');

        if (!(is_object($theme_object))) {
            $theme_object = get_page_by_title('global', OBJECT, 'custom_theme');
        }

        // Check that theme object was returned
        if (is_object($theme_object)) {
            $theme_post_id = $theme_object->ID;
        } else {
            $global_theme_post = [
                'post_title'     => 'global',
                'post_name'      => 'global',
                'post_status'    => 'publish',
                'post_author'    => 1,
                'post_type'      => 'custom_theme',
                'menu_order'     => 0,
                'comment_status' => 'closed',
                'ping_status'    => 'closed',
            ];

            $theme_post_id = wp_insert_post($global_theme_post);
        }

        $post_meta_fields = [
            '_primary_hex_color'                                 => '#f05a30',
            '_headline_hex_color'                                => '#f05a30',
            '_copy_hex_color'                                    => '#8e8e8e',
            '_links_hex_color'                                   => '#f05a30',
            '_button_text_hex_color'                             => '#ffffff',
            '_button_color_hex_color'                            => '#f05a30',
            '_body_background_hex_color'                         => '#f8f8fb',
            '_panel_background_hex_color'                        => '#ffffff',
            '_footer_background_hex_color'                       => '#f0f0f3',
            '_footer_text_hex_color'                             => '#b2b2b2',
            '_info_bar_background_hex_color'                     => '#f05a30',
            '_info_bar_text_hex_color'                           => '#ffffff',
            '_panel_box_shadow'                                  => 1,
            '_wifi_heading'                                      => 'Enter your phone number!',
            '_wifi_wifi_submit_button_text'                      => 'Get STARTED!',
            '_doi_heading'                                       => 'Subscription Confirmation for {msisdn}',
            '_doi_text'                                          => '{service_cost} subscription service.',
            '_doi_doi_submit_button_text'                        => 'Yes',
            '_doi_doi_cancel_button_text'                        => 'Cancel',
            '_success-text_heading'                              => 'We have sent you an sms!',
            '_success-text_text'                                 => 'Reply YES in the SMS to get started!',
            '_success-text_try_again_button_text'                => 'Try Again',
            '_success-ussd_heading'                              => 'Please wait for the pop-up message!',
            '_success-ussd_text'                                 => 'Reply 1 to confirm your subscription.',
            '_success-ussd_replied_button_text'                  => 'I\'ve Replied',
            '_success-ussd_try_again_button_text'                => 'Try Again',
            '_error_heading'                                     => 'There was a problem creating your subscription',
            '_error_try_again_button_text'                       => 'Try Again',
            '_forbidden_heading'                                 => 'Oh no!',
            '_forbidden_text'                                    => 'Sorry, but {service_title} is not available on your mobile network',
            '_declined_heading'                                  => 'You’ve declined the subscription.',
            '_declined_text'                                     => 'Did you make a mistake?',
            '_declined_try_again_button_text'                    => 'Try Again',
            '_disable-wifi_heading'                              => 'Please switch OFF your WiFi!',
            '_disable-wifi_text'                                 => 'To access this service you need to use your mobile data connection.',
            '_disable-wifi_refresh_button_text'                  => 'Refresh',
            '_blocked_heading'                                   => 'Whoops!',
            '_blocked_text'                                      => 'The device you are using is not allowed to access this service.',
            '_content-blocked_heading'                           => 'Your number is blocking content services',
            '_content-blocked_text'                              => 'Please disable content block and try again. Dial {support_number} for support.',
            '_insufficient-funds_heading'                        => 'You have insufficient airtime to subscribe to this service. Please recharge and try again.',
            '_insufficient-funds_try_again_button_text'          => 'Try Again',
            '_session-timeout_heading'                           => 'Your Session Timed Out',
            '_session-timeout_try_again_button_text'             => 'Try Again',
            '_thank-you_heading'                                 => 'Thank You',
            '_thank-you_text'                                    => 'You have been successfully subscribed!',
            '_thank-you_continue_button_text'                    => 'Continue to {service_title}',
            '_thank-you-forced_heading'                          => 'Thank You',
            '_thank-you-forced_text'                             => 'Thank you for subscribing, you\'ll soon receive an SMS with details of your subscription.',
            '_text-subscribed_heading'                           => 'Oops!',
            '_text-subscribed_text'                              => 'We have already sent you a text to confirm your subscription.',
            '_text-subscribed_continue_button_text'              => 'Continue',
            '_text-subscribed-interstitial_heading'              => 'Already Subscribed!',
            '_text-subscribed-interstitial_text'                 => 'Hey, looks like you\'re already subscribed to that service. Here is a similar service that you may like.',
            '_text-subscribed-interstitial_continue_button_text' => 'Yes',
            '_duplicate-request_heading'                         => 'Oops!',
            '_duplicate-request_text'                            => 'We have already sent you a text to confirm your subscription.',
            '_thank-you-cross-sell_text'                         => 'You might also like some of these services',
            '_thank-you-cross-sell_continue_button_text'         => 'Continue to {service_title}',
            '_pending_heading'                                   => 'Oh that’s awkward',
            '_pending_text'                                      => 'We’ve received your confirmation, however it’s taking a little long to confirm. Please wait for your welcome message.',
            '_pending_try_again_button_text'                     => 'Try Again',
            '_forced-error_heading'                              => 'Sorry, but this service is not available for once-off purchase at this time.',
            '_forced-error_try_again_button_text'                => 'Back',
            '_pre-doi_heading'                                   => 'Subscription Confirmation for {msisdn}',
            '_pre-doi_text'                                      => '{service_cost} subscription service.',
            '_pre-doi_doi_submit_button_text'                    => 'Yes',
            '_pre-doi_doi_cancel_button_text'                    => 'Cancel',
        ];

        // Insert post meta fields
        foreach ($post_meta_fields as $field_name => $field_value) {
            if (strlen(get_post_meta($theme_post_id, $field_name, true)) < 1) {
                update_post_meta($theme_post_id, $field_name, $field_value);
            }
        }
    }
}
