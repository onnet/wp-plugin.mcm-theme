<?php
/** @noinspection PhpIncludeInspection */

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @package    theme
 * @subpackage theme/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @package    theme
 * @subpackage theme/includes
 * @author     Hyve Mobile <glen@hyvemobile.co.za>
 */
class Mcm_Theme
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      theme_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->version = defined('theme_VERSION') ? theme_VERSION : '2.1.1';
        $this->plugin_name = 'mcm-theme';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - theme_Loader. Orchestrates the hooks of the plugin.
     * - theme_i18n. Defines internationalization functionality.
     * - theme_Admin. Defines all hooks for the admin area.
     * - theme_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {
        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-theme-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-theme-i18n.php';

        /**
         * Required Helper files
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-theme-helpers.php';
        foreach (glob(plugin_dir_path(dirname(__FILE__)) . 'includes/helpers/*.php') as $filename) {
            require_once "{$filename}";
        }

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-mcm-theme-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-mcm-theme-admin-settings.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-mcm-theme-public.php';

        $this->loader = new theme_Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the theme_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {
        $plugin_i18n = new theme_i18n();
        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {
        $plugin_admin = new Mcm_Theme_Admin($this->get_plugin_name(), $this->get_version());
        $plugin_admin_settings = new Mcm_Theme_Admin_Settings($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        // Add Theme Actions
        $this->loader->add_action('init', $plugin_admin, 'theme_post_type');
        $this->loader->add_action('admin_menu', $plugin_admin, 'register_menu_pages');
        $this->loader->add_action('admin_menu', $plugin_admin, 'remove_aggregator_sub_menu_item');
        $this->loader->add_action('add_meta_boxes', $plugin_admin, 'register_meta_boxes');
        $this->loader->add_action('save_post_custom_theme', $plugin_admin, 'save_post', 10, 3);
        $this->loader->add_action('wp_trash_post', $plugin_admin, 'remove_custom_theme_post_id');
//        $this->loader->add_action('service_theme_redirect', $plugin_admin, 'service_theme_redirect');

        // Settings Actions
        $this->loader->add_action('admin_menu', $plugin_admin_settings, 'add_options_page');
        $this->loader->add_action('admin_init', $plugin_admin_settings, 'register_service_settings');
        $this->loader->add_action('admin_init', $plugin_admin_settings, 'update_option_hooks');

        // Taxonomies
        $this->loader->add_action('init', $plugin_admin, 'theme_type_taxonomy', 0);
//        $this->loader->add_action( 'restrict_manage_posts', $plugin_admin, 'filter_theme_level_taxonomies' , 10, 2);

        // Add Admin Filters
        $this->loader->add_filter('custom_menu_order', $plugin_admin, 'change_custom_theme_submenu_order');
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {
        $plugin_public = new Mcm_Theme_Public($this->get_plugin_name(), $this->get_version());

        // Content Template File
        $this->loader->add_filter('csc_display_template_file', $plugin_public, 'display_template_file', 10, 1);

        // Add Theme Public Actions
        $this->loader->add_action('display_theme_banner', $plugin_public, 'display_theme_banner');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles', 98);
        $this->loader->add_action('wp_head', $plugin_public, 'theme_styles', 99);
        $this->loader->add_action('wp_head', $plugin_public, 'template_copy', 99);
        $this->loader->add_action('template_redirect', $plugin_public, 'set_theme_banner_id');
        $this->loader->add_action('set_theme_query_vars', $plugin_public, 'set_theme_query_vars');
        $this->loader->add_action('theme_flow_template_part', $plugin_public, 'theme_flow_template_part', 10);
        $this->loader->add_action('show_network_logo', $plugin_public, 'show_network_logo', 10);
        $this->loader->add_filter('get_service_network_placeholder', $plugin_public, 'get_service_network_placeholder', 10);

        // Add Theme Public Filters
        $this->loader->add_filter('query_vars', $plugin_public, 'add_query_vars', 10);
        $this->loader->add_filter('mcm_set_theme_hexes', $plugin_public, 'mcm_set_theme_hexes', 10);
        $this->loader->add_filter('get_theme_banner', $plugin_public, 'get_theme_banner', 10, 2);
        $this->loader->add_filter('get_template_copy', $plugin_public, 'get_template_copy', 10, 2);
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     * @since     1.0.0
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @return    theme_Loader    Orchestrates the hooks of the plugin.
     * @since     1.0.0
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @return    string    The version number of the plugin.
     * @since     1.0.0
     */
    public function get_version()
    {
        return $this->version;
    }

}
