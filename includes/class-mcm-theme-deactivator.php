<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    theme
 * @subpackage theme/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    theme
 * @subpackage theme/includes
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class theme_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'template_fields';
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
        delete_option("my_plugin_db_version");

        $table_name = $wpdb->prefix . 'templates';
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
        delete_option("my_plugin_db_version");

        $table_name = $wpdb->prefix . 'fields';
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
        delete_option("my_plugin_db_version");
	}

}
