<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    theme
 * @subpackage theme/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    theme
 * @subpackage theme/includes
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class theme_Activator
{
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        global $wpdb;

        // Templates
        $table_name = $wpdb->prefix . 'templates';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`                        INT(10)  NOT NULL AUTO_INCREMENT,
          `name`                      VARCHAR(50) NOT NULL,
          `slug`                      VARCHAR(50) NOT NULL,
          `template_key`              VARCHAR(10),
          `description`               VARCHAR(250),
          `created_at` DATETIME       NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);

        // Fields
        $table_name = $wpdb->prefix . 'fields';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`                  INT(10) NOT NULL AUTO_INCREMENT,
          `name`                VARCHAR(50) NOT NULL,
          `slug`                VARCHAR(50) NOT NULL,
          `description`         VARCHAR(250),
          `created_at`          DATETIME   NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);

        // Fields
        $table_name = $wpdb->prefix . 'template_fields';
        $sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
          `id`          INT(10)   NOT NULL AUTO_INCREMENT,
          `template_id` INT(10)   NOT NULL,
          `field_id`    INT(10)   NOT NULL,
          `created_at`  DATETIME  NOT NULL,
          PRIMARY KEY (`id`)
        )
          ENGINE = InnoDB";
        dbDelta($sql);


        // Populate Templates Table
        $created_at = date("Y-m-d H:i:s", time());
        $table = $wpdb->prefix . 'templates';

        $templates = [
            [
                'id'           => 1,
                'name'         => 'Wifi',
                'slug'         => 'enter-msisdn',
                'template_key' => '1',
                'description'  => 'Default template shown when no msisdn is detected',
            ],
            [
                'id'           => 2,
                'name'         => 'DOI',
                'slug'         => 'internal-doi',
                'template_key' => '1.1',
                'description'  => 'Internal DOI',
            ],
            [
                'id'           => 3,
                'name'         => 'Post DOI Confirm',
                'slug'         => 'internal-post-doi-confirm',
                'template_key' => '1.2',
                'description'  => 'Additional confirmation template shown before the internal doi template',
            ],
            [
                'id'           => 4,
                'name'         => 'Success Text',
                'slug'         => 'success-text',
                'template_key' => '2.1',
                'description'  => 'Successful Text Subscription',
            ],
            [
                'id'           => 5,
                'name'         => 'Success USSD',
                'slug'         => 'success-ussd',
                'template_key' => '2.2',
                'description'  => 'Successful USSD Subscription',
            ],
            [
                'id'           => 6,
                'name'         => 'Success Internal DOI',
                'slug'         => 'success-internal-doi',
                'template_key' => '2.3',
                'description'  => 'Successful Internal DOI Subscription',
            ],
            [
                'id'           => 7,
                'name'         => 'Success External DOI',
                'slug'         => 'success-external-doi',
                'template_key' => '2.4',
                'description'  => 'Successful External DOI Subscription',
            ],
            [
                'id'           => 8,
                'name'         => 'Error',
                'slug'         => 'error',
                'template_key' => '3',
                'description'  => 'Default Error Template',
            ],
            [
                'id'           => 9,
                'name'         => 'Forbidden',
                'slug'         => 'error-forbidden',
                'template_key' => '3.1',
                'description'  => 'Forbidden on this network',
            ],
            [
                'id'           => 10,
                'name'         => 'Declined',
                'slug'         => 'error-declined',
                'template_key' => '3.2',
                'description'  => 'Declined subscription',
            ],
            [
                'id'           => 11,
                'name'         => 'Thank You Text',
                'slug'         => 'thank-you-text',
                'template_key' => '4.1',
                'description'  => 'Thank You for subscribing text',
            ],
            [
                'id'           => 12,
                'name'         => 'Thank You Web',
                'slug'         => 'thank-you-web',
                'template_key' => '4.2',
                'description'  => 'Thank You for subscribing web',
            ],
            [
                'id'           => 13,
                'name'         => 'Thank You App',
                'slug'         => 'thank-you-app',
                'template_key' => '4.3',
                'description'  => 'Thank You for subscribing app',
            ],
            [
                'id'           => 14,
                'name'         => 'Already Subscribed',
                'slug'         => 'already-subscribed',
                'template_key' => '5',
                'description'  => 'User is already subscribed',
            ],
            [
                'id'           => 15,
                'name'         => 'Duplicate Subscription',
                'slug'         => 'duplicate-request',
                'template_key' => '5.1',
                'description'  => 'SMS already sent to user',
            ],
        ];

        foreach ($templates as $template) {
            $template['created_at'] = $created_at;
            $wpdb->insert($table, $template);
        }

        // Populate Fields Table
        $table = $wpdb->prefix . 'fields';

        $fields = [
            [
                'id'          => 1,
                'name'        => 'Heading',
                'slug'        => 'heading',
                'description' => 'The main heading text',
            ],
            [
                'id'          => 2,
                'name'        => 'Text',
                'slug'        => 'text',
                'description' => 'The smaller paragraph text',
            ],
            [
                'id'          => 3,
                'name'        => 'WiFI Submit Button Text',
                'slug'        => 'wifi_submit_button_text',
                'description' => 'The button text for the wifi submit button',
            ],
            [
                'id'          => 4,
                'name'        => 'Internal DOI Submit Button Text',
                'slug'        => 'doi_submit_button_text',
                'description' => 'The button to submit',
            ],
            [
                'id'          => 5,
                'name'        => 'Internal DOI Cancel Button Text',
                'slug'        => 'doi_cancel_button_text',
                'description' => 'The button to cancel',
            ],
            [
                'id'          => 6,
                'name'        => 'Try Again Button Text',
                'slug'        => 'try_again_button_text',
                'description' => 'The text for the try again button',
            ],
            [
                'id'          => 7,
                'name'        => 'I\'ve Replied Button Text',
                'slug'        => 'replied_button_text',
                'description' => 'The text for the I\'ve replied button',
            ],
            [
                'id'          => 8,
                'name'        => 'Refresh Button Text',
                'slug'        => 'refresh_button_text',
                'description' => 'The text for the refresh button',
            ],
            [
                'id'          => 9,
                'name'        => 'Continue Button Text',
                'slug'        => 'continue_button_text',
                'description' => 'The text for the continue button',
            ],
        ];

        foreach ($fields as $field) {
            $field['created_at'] = $created_at;
            $wpdb->insert($table, $field);
        }


        // Populate Template Fields Table
        $table = $wpdb->prefix . 'template_fields';

        $template_fields = [
            // WiFI Template (1)
            [
                'template_id' => 1,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 1,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 1,
                'field_id'    => 3, // Wifi Submit Button Text
            ],

            // DOI Template (1.1)
            [
                'template_id' => 2,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 2,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 2,
                'field_id'    => 4, // DOI Submit Button Text
            ],
            [
                'template_id' => 2,
                'field_id'    => 5, // DOI Cancel Button Text
            ],

            // Pre DOI Template (1.2)
            [
                'template_id' => 3,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 3,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 3,
                'field_id'    => 4, // DOI Submit Button Text
            ],
            [
                'template_id' => 3,
                'field_id'    => 5, // DOI Cancel Button Text
            ],

            // Success Text Template (2.1)
            [
                'template_id' => 4,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 4,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 4,
                'field_id'    => 9, // Continue Button Text
            ],

            // Success USSD Template (2.2)
            [
                'template_id' => 5,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 5,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 5,
                'field_id'    => 6, // Try Again Button Text
            ],
            [
                'template_id' => 5,
                'field_id'    => 7, // I've replied Button Text
            ],

            // Success Internal DOI Template (2.3)
            [
                'template_id' => 6,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 6,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 6,
                'field_id'    => 9, // Continue Button Text
            ],

            // Success External DOI Template (2.4)
            [
                'template_id' => 7,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 7,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 7,
                'field_id'    => 9, // Continue Button Text
            ],

            // Error Template (3)
            [
                'template_id' => 8,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 8,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 8,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Forbidden Template (3.1)
            [
                'template_id' => 9,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 9,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 9,
                'field_id'    => 9, // Continue Button Text
            ],

            // Declined Template (3.2)
            [
                'template_id' => 10,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 10,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 10,
                'field_id'    => 6, // Try Again Button Text
            ],

            // Thank You Text (4.1)
            [
                'template_id' => 11,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 11,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 11,
                'field_id'    => 9, // Continue Button Text
            ],

            // Thank You Text (4.2)
            [
                'template_id' => 12,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 12,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 12,
                'field_id'    => 9, // Continue Button Text
            ],

            // Thank You Text (4.3)
            [
                'template_id' => 13,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 13,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 13,
                'field_id'    => 9, // Continue Button Text
            ],

            // Already Subscribed Template (5)
            [
                'template_id' => 14,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 14,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 14,
                'field_id'    => 9, // Continue Button Text
            ],

            // Duplicate Request Template (5.1)
            [
                'template_id' => 15,
                'field_id'    => 1, // Heading
            ],
            [
                'template_id' => 15,
                'field_id'    => 2, // Body Text
            ],
            [
                'template_id' => 15,
                'field_id'    => 9, // Continue Button Text
            ],
        ];

        foreach ($template_fields as $template_field) {
            $template_field['created_at'] = $created_at;
            $wpdb->insert($table, $template_field);
        }


        // Insert default copy
        $table = $wpdb->prefix . 'template_fields';
        $theme_post_id = null;

        // Check if a default/global theme already exists
        $theme_object = get_page_by_title('default', OBJECT, 'custom_theme');

        if (!(is_object($theme_object))) {
            $theme_object = get_page_by_title('global', OBJECT, 'custom_theme');
        }

        // Check that theme object was returned
        if (is_object($theme_object)) {
            $theme_post_id = $theme_object->ID;
        } else {
            $global_theme_post = [
                'post_title'     => 'global',
                'post_name'      => 'global',
                'post_status'    => 'publish',
                'post_author'    => 1,
                'post_type'      => 'custom_theme',
                'menu_order'     => 0,
                'comment_status' => 'closed',
                'ping_status'    => 'closed',
            ];

            $theme_post_id = wp_insert_post($global_theme_post);
        }

        $post_meta_fields = [
            '_primary_hex_color'                                => '#f05a30',
            '_headline_hex_color'                               => '#f05a30',
            '_copy_hex_color'                                   => '#8e8e8e',
            '_links_hex_color'                                  => '#f05a30',
            '_button_text_hex_color'                            => '#ffffff',
            '_button_color_hex_color'                           => '#f05a30',
            '_body_background_hex_color'                        => '#f8f8fb',
            '_panel_background_hex_color'                       => '#ffffff',
            '_footer_background_hex_color'                      => '#f0f0f3',
            '_footer_text_hex_color'                            => '#b2b2b2',
            '_info_bar_background_hex_color'                    => '#f05a30',
            '_info_bar_text_hex_color'                          => '#ffffff',
            '_panel_box_shadow'                                 => 1,
            '_enter-msisdn_heading'                             => 'Enter your number below to subscribe to {service_title} for {service_cost}',
            '_enter-msisdn_text'                                => '',
            '_enter-msisdn_wifi_submit_button_text'             => 'Get STARTED!',
            '_internal-doi_heading'                             => 'Click YES below to subscribe to {service_title}',
            '_internal-doi_text'                                => 'This services costs {service_cost}',
            '_internal-doi_doi_submit_button_text'              => 'Yes',
            '_internal-doi_doi_cancel_button_text'              => 'Cancel',
            '_internal-post-doi-confirm_heading'                => 'Are you sure you want to subscribe to {service_title}?',
            '_internal-post-doi-confirm_text'                   => '',
            '_internal-post-doi-confirm_doi_submit_button_text' => 'Yes',
            '_internal-post-doi-confirm_doi_cancel_button_text' => 'Cancel',
            '_success-text_heading'                             => 'Thank you!',
            '_success-text_text'                                => 'You will receive a confirmation SMS shortly. Reply to the SMS to subscribe',
            '_success-text_continue_button_text'                => 'Back to Home',
            '_success-ussd_heading'                             => 'Thank you!',
            '_success-ussd_text'                                => 'A confirmation message will be sent to your phone in a few seconds. Reply YES and then click the button to continue',
            '_success-ussd_replied_button_text'                 => 'I\'ve Replied',
            '_success-ussd_try_again_button_text'               => 'I haven\'t received anything yet',
            '_success-internal-doi_heading'                     => 'Thank you!',
            '_success-internal-doi_text'                        => '',
            '_success-internal-doi_continue_button_text'        => 'Continue to {service_title}',
            '_success-external-doi_heading'                     => 'Thank you!',
            '_success-external-doi_text'                        => '',
            '_success-external-doi_continue_button_text'        => 'Continue to {service_title}',
            '_error_heading'                                    => 'Oops!',
            '_error_text'                                       => 'Something went wrong',
            '_error_try_again_button_text'                      => 'Try Again',
            '_error-forbidden_heading'                          => 'Oops!',
            '_error-forbidden_text'                             => 'Sorry, but this service is not available on your mobile network',
            '_error-forbidden_continue_button_text'             => 'Back to Home',
            '_error-declined_heading'                           => 'Oops!',
            '_error-declined_text'                              => 'You have declined the subscription! Did you make a mistake?',
            '_error-declined_try_again_button_text'             => 'Try Again',
            '_thank-you-text_heading'                           => 'Thank you!',
            '_thank-you-text_text'                              => 'You have been successfully subscribed!',
            '_thank-you-text_continue_button_text'              => 'Continue to {service_title}',
            '_thank-you-web_heading'                            => 'Thank You',
            '_thank-you-web_text'                               => 'You have been successfully subscribed!',
            '_thank-you-web_continue_button_text'               => 'Continue to {service_title}',
            '_thank-you-app_heading'                            => 'Thank You',
            '_thank-you-app_text'                               => 'You have been successfully subscribed!',
            '_thank-you-app_continue_button_text'               => 'Continue to {service_title}',
            '_already-subscribed_heading'                       => 'Nice!',
            '_already-subscribed_text'                          => 'You are already subscribed! If you haven’t received your content, please ensure you have enough airtime in your account to pay for the service.',
            '_already-subscribed_continue_button_text'          => 'Continue to {service_title}',
            '_duplicate-request_heading'                        => 'Oops!',
            '_duplicate-request_text'                           => 'We have already sent you a text to confirm your subscription.',
            '_duplicate-request_continue_button_text'           => 'Back to Home',
        ];

        // Insert post meta fields
        foreach ($post_meta_fields as $field_name => $field_value) {
            if (strlen(get_post_meta($theme_post_id, $field_name, true)) < 1) {
                update_post_meta($theme_post_id, $field_name, $field_value);
            }
        }
    }
}
