<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    theme
 * @subpackage theme/admin
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class Mcm_Theme_Admin
{
    /**
     * The ID of this plugin.
     *
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * The post type key
     *
     * @var string The key for the post type
     */
    private $theme_post_type_key = 'custom_theme';
    private $templating_post_type_key = 'custom_template';

    /**
     * Default colour used for component
     *
     * @var string
     */
    private $default_color;

    private $theme_services_post_type;

    /**
     * Initialize the class and set its properties.
     *
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version     The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->default_color = '#ffffff';
        $this->theme_services_post_type = get_option('theme_services_post_type');
    }

    /**
     * Register the stylesheets for the admin area.
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/theme-admin.css', [], '1.0', 'all');
        wp_enqueue_style($this->plugin_name . '-ui', plugin_dir_url(__FILE__) . 'css/jquery-ui.min.css', [], '1.0', 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     */
    public function enqueue_scripts()
    {
        $screen = get_current_screen();
        if ('custom_theme' === $screen->post_type) {
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/theme-admin.js', ['jquery'], '1.0', false);
            wp_enqueue_script($this->plugin_name . '-tmpl', plugin_dir_url(__FILE__) . 'js/jquery.tmpl.min.js', ['jquery'], $this->version, true);
            wp_enqueue_script($this->plugin_name . '-ui', plugin_dir_url(__FILE__) . 'js/jquery-ui.min.js', ['jquery'], $this->version, true);
        }
    }

    public function admin_menu()
    {
        add_management_page(__('Theme Management'), __('Theme'), 'manage_options', $this->plugin_name, [$this, 'theme_dashboard']);
    }

    /**
     * Save the post data
     *
     * @param int      $post_id
     * @param \WP_Post $post
     * @param bool     $update
     *
     * @return bool
     */
    public function save_post($post_id, $post, $update)
    {
        // Validation
        if (!wp_is_theme_really_saving($post_id, $post)) {
            return;
        }

        // If this isn't a 'mcm-theme' post, don't update it.
        if ('custom_theme' !== get_post_type($post_id)) {
            return;
        }

        // Filter on failure
        $condition = [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_NULL_ON_FAILURE,
        ];

        // Filter hex meta keys
        $hex_meta_keys = [
            'primary_hex_color'              => $condition,
            'headline_hex_color'             => $condition,
            'copy_hex_color'                 => $condition,
            'links_hex_color'                => $condition,
            'button_text_hex_color'          => $condition,
            'button_color_hex_color'         => $condition,
            'body_background_hex_color'      => $condition,
            'panel_background_hex_color'     => $condition,
            'panel_box_shadow'               => $condition,
            'footer_background_hex_color'    => $condition,
            'footer_text_hex_color'          => $condition,
            'info_bar_background_hex_color'  => $condition,
            'info_bar_text_hex_color'        => $condition,
            'theme_active'                   => $condition,
            'hide_logo'                      => $condition,
            'service_theme_banner_full_size' => $condition,
        ];

        // Filter template meta keys
        $templates = $this->get_templates();
        $template_meta_keys = [];

        foreach ($templates as $template_name => $template_fields) {
            foreach ($template_fields as $k => $v) {
                $template_meta_keys[$v['postmeta_field']] = $condition;
            }
        }

        // Filter service meta keys
        $service_theme_keys = [
            'service_theme_post_id'              => $condition,
            'service_theme_banner_url'           => $condition,
            'service_theme_banner_attachment_id' => $condition,
        ];

        $hex_data = filter_input_array(INPUT_POST, $hex_meta_keys);
        $service_theme_data = filter_input_array(INPUT_POST, $service_theme_keys);
        $template_data = filter_input_array(INPUT_POST, $template_meta_keys);

        // Save hex post meta
        foreach ($hex_meta_keys as $meta_key => $condition) {
            $value = $hex_data[$meta_key];

            // Only apply to fields containing the 'color' keyword and hex values only apply to them
            if (strpos($meta_key, 'color') !== false) {
                preg_match('/#([a-f0-9]{6}){1,2}\b/i', $hex_data[$meta_key], $matches); // Validate hex color value
                $value = (isset($matches[0])) ? $matches[0] : $this->default_color;
            }

            update_post_meta($post_id, '_' . $meta_key, $value);
        }

        // Save template post meta
        foreach ($template_meta_keys as $meta_key => $condition) {
            $value = $template_data[$meta_key];
            update_post_meta($post_id, '_' . $meta_key, $value);
        }

        // Update checkboxes
        update_post_meta($post_id, '_panel_box_shadow', isset($_POST['panel_box_shadow']) ? 1 : 0);
        update_post_meta($post_id, '_theme_active', isset($_POST['theme_active']) ? 1 : 0);
        update_post_meta($post_id, '_hide_logo', isset($_POST['hide_logo']) ? 1 : 0);
        update_post_meta($post_id, '_service_theme_banner_full_size', isset($_POST['service_theme_banner_full_size']) ? 1 : 0);

        // Save service theme post meta
        foreach ($service_theme_keys as $meta_key => $condition) {
            update_post_meta($post_id, '_' . $meta_key, $service_theme_data[$meta_key]);

            // Add the theme post id to the service post meta if it is a service specific theme
            if ($meta_key == 'service_theme_post_id') {
                if (strlen($service_theme_data[$meta_key]) > 0) {
                    update_post_meta(
                        $service_theme_data[$meta_key],
                        '_custom_theme_post_id',
                        get_the_ID()
                    );
                }
            }
        }
    }

    /**
     * Register Menu Pages
     */
    public function register_menu_pages()
    {
        add_submenu_page('edit.php?post_type=custom_theme', __('Global'), __('Global'), 'publish_posts', 'edit.php?post_type=custom_theme&theme_type=global');
        add_submenu_page('edit.php?post_type=custom_theme', __('Billing Channel'), __('Billing Channel'), 'publish_posts', 'edit.php?post_type=custom_theme&theme_type=billing-channel');
        add_submenu_page('edit.php?post_type=custom_theme', __('Service'), __('Service'), 'publish_posts', 'edit.php?post_type=custom_theme&theme_type=service');
    }

    /**
     * Theme Post Type
     */
    public function theme_post_type()
    {
        $labels = [
            'name'                  => _x('Themes', 'Post Type General Name', $this->plugin_name),
            'singular_name'         => _x('Theme', 'Post Type Singular Name', $this->plugin_name),
            'menu_name'             => __('Themes', $this->plugin_name),
            'name_admin_bar'        => __('Themes', $this->plugin_name),
            'archives'              => __('Themes Archives', $this->plugin_name),
            'attributes'            => __('Themes Attributes', $this->plugin_name),
            'parent_item_colon'     => __('Parent Themes:', $this->plugin_name),
            'all_items'             => __('All', $this->plugin_name),
            'add_new_item'          => __('Add New Theme', $this->plugin_name),
            'add_new'               => __('Add New', $this->plugin_name),
            'new_item'              => __('New Theme', $this->plugin_name),
            'edit_item'             => __('Edit Theme', $this->plugin_name),
            'update_item'           => __('Update Theme', $this->plugin_name),
            'view_item'             => __('View Theme', $this->plugin_name),
            'view_items'            => __('View Themes', $this->plugin_name),
            'search_items'          => __('Search Themes', $this->plugin_name),
            'not_found'             => __('Not found', $this->plugin_name),
            'not_found_in_trash'    => __('Not found in Trash', $this->plugin_name),
            'featured_image'        => __('Featured Image', $this->plugin_name),
            'set_featured_image'    => __('Set featured image', $this->plugin_name),
            'remove_featured_image' => __('Remove featured image', $this->plugin_name),
            'use_featured_image'    => __('Use as featured image', $this->plugin_name),
            'insert_into_item'      => __('Insert into Themes', $this->plugin_name),
            'uploaded_to_this_item' => __('Uploaded to Themes', $this->plugin_name),
            'items_list'            => __('Themes list', $this->plugin_name),
            'items_list_navigation' => __('Themes list navigation', $this->plugin_name),
            'filter_items_list'     => __('Filter Themes list', $this->plugin_name),
        ];

        $args = [
            'label'               => __('Themes', $this->plugin_name),
            'description'         => __('Themes Description', $this->plugin_name),
            'labels'              => $labels,
            'supports'            => ['title'],
            'taxonomies'          => ['aggregator'],
            'hierarchical'        => false,
            'public'              => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 30,
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => false,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => false,
            'rewrite'             => false,
            'capability_type'     => 'page',
            'show_in_rest'        => false,
            'rest_base'           => 'custom_theme',
        ];

        register_post_type($this->theme_post_type_key, $args);
    }

    /**
     * Club Type Taxonomy
     */
    public function theme_type_taxonomy()
    {
        $labels = [
            'name'                       => __('Theme Type'),
            'singular_name'              => __('Theme Type'),
            'menu_name'                  => __('Theme Types'),
            'all_items'                  => __('All Theme Types'),
            'parent_item'                => __('Parent Theme Type'),
            'parent_item_colon'          => __('Parent Theme Type:'),
            'new_item_name'              => __('New Theme Type'),
            'add_new_item'               => __('Add New Theme Type'),
            'edit_item'                  => __('Edit Theme Type'),
            'update_item'                => __('Update Theme Type'),
            'view_item'                  => __('View Theme Type'),
            'separate_items_with_commas' => __('Separate Theme Types with commas'),
            'add_or_remove_items'        => __('Add or remove Theme Types'),
            'choose_from_most_used'      => __('Choose from the most used Theme Types'),
            'popular_items'              => __('Popular Theme Types'),
            'search_items'               => __('Search Theme Types'),
            'not_found'                  => __('No Theme Types Found'),
            'no_terms'                   => __('No Theme Types'),
            'items_list'                 => __('Theme Type list'),
            'items_list_navigation'      => __('Theme Type list navigation'),
        ];


        $args = [
            'labels'            => $labels,
            'hierarchical'      => true,
            'public'            => true,
            'show_ui'           => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud'     => false,
            'show_in_menu'      => false,
        ];

        register_taxonomy('theme_type', ['custom_theme'], $args);

        // Insert default terms
        $terms = [
            [
                'name' => 'Global',
                'slug' => 'global',
            ],
            [
                'name' => 'Billing Channel',
                'slug' => 'billing-channel',
            ],
            [
                'name' => 'Service',
                'slug' => 'service',
            ],
        ];

        foreach ($terms as $kye => $term) {
            if (!term_exists($term['name'], 'theme_type')) {
                wp_insert_term(
                    $term['name'], 'theme_type', ['slug' => $term['slug']]
                );
            }
        }
    }

    /**
     * @param $post_type
     * @param $which
     */
    function filter_theme_level_taxonomies($post_type, $which)
    {

        // Apply this only on a specific post type
        if ('custom_theme' !== $post_type) {
            return;
        }

        $taxonomies = ['template_level'];

        foreach ($taxonomies as $taxonomy_slug) {
            $taxonomy_obj = get_taxonomy($taxonomy_slug);
            $taxonomy_name = $taxonomy_obj->labels->name;
            $terms = get_terms($taxonomy_slug);

            // Display filter HTML
            echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
            echo '<option value="">' . sprintf(esc_html__('Show All %s', 'text_domain'), $taxonomy_name) . '</option>';
            foreach ($terms as $term) {
                printf(
                    '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                    $term->slug,
                    ((isset($_GET[$taxonomy_slug]) && ($_GET[$taxonomy_slug] == $term->slug)) ? ' selected="selected"' : ''),
                    $term->name,
                    $term->count
                );
            }
            echo '</select>';
        }

    }

    /**
     * Add club settings meta boxes
     */
    public function register_meta_boxes()
    {
        add_meta_box('template-accordion', __('Template Copy Customization'), [$this, 'meta_box_template_accordion'], 'custom_theme');

        // Service campaign meta box
        add_meta_box('service-theme', __('Theme'), [$this, 'meta_box_service_theme'], $this->theme_services_post_type);

        // Theme Metaboxes
        if ($this->is_service()) {
            add_meta_box('service-details', __('Service Details'), [$this, 'meta_box_service_details'], 'custom_theme');
            add_meta_box('service-banner', __('Service Banner'), [$this, 'meta_box_service_banner'], 'custom_theme');
        }

        add_meta_box('theme', __('Theme Customization'), [$this, 'meta_box_theme'], 'custom_theme');
    }

    /**
     * Arbitrage Meta Box
     *
     * @param $post
     */
    public function meta_box_template_accordion()
    {
        $this->include_partial('template', 'accordion', [
            'theme_postmeta' => get_post_meta(get_the_ID()),
            'templates'      => $this->get_templates(),
        ]);
    }

    /**
     * Arbitrage Meta Box
     *
     * @param $post
     */
    public function meta_box_service_theme($post)
    {
        $theme_post_id = get_post_meta(get_the_ID(), '_custom_theme_post_id', true);
        $theme_link = $this->get_theme_link();

        $this->include_partial('service', 'theme', [
            'post'          => $post,
            'theme_post_id' => $theme_post_id,
            'theme_link'    => $theme_link,
        ]);
    }

    /**
     * Service Banner Meta Box
     */
    public function meta_box_service_banner()
    {
        $service_post_id = $this->get_service_post_id();

        $this->include_partial('meta-box', 'service-banner', ['service_post_id' => $service_post_id,]);
    }

    /**
     * Service Details Meta Box
     */
    public function meta_box_service_details()
    {
        $service_post_id = $this->get_service_post_id();
        $service_title = (!is_null($service_post_id)) ? (get_the_title($service_post_id)) : null;
        $service_link = $this->get_service_link($service_post_id);

        $this->include_partial('meta-box', 'service-details', [
            'service_post_id' => $service_post_id,
            'service_link'    => $service_link,
            'service_title'   => $service_title,
        ]);
    }

    /**
     * Primary Meta Box
     */
    public function meta_box_primary_palette()
    {
        $primary_hex_color = (get_post_meta(get_the_ID(), '_primary_hex_color', true)) ? get_post_meta(get_the_ID(), '_primary_hex_color', true) : $this->default_color;
        $filtered_aggregators = $this->get_filtered_aggregators();

//        dd($filtered_aggregators);

        $this->include_partial('meta-box', 'primary-palette', [
            'primary_hex_color'    => $primary_hex_color,
            'filtered_aggregators' => $filtered_aggregators,
        ]);
    }

    /**
     * Typography Meta Box
     */
    public function meta_box_typography()
    {
        $headline_hex_color = (get_post_meta(get_the_ID(), '_headline_hex_color', true)) ? get_post_meta(get_the_ID(), '_headline_hex_color', true) : $this->default_color;
        $copy_hex_color = (get_post_meta(get_the_ID(), '_copy_hex_color', true)) ? get_post_meta(get_the_ID(), '_copy_hex_color', true) : $this->default_color;
        $links_hex_color = (get_post_meta(get_the_ID(), '_links_hex_color', true)) ? get_post_meta(get_the_ID(), '_links_hex_color', true) : $this->default_color;

        $this->include_partial('meta-box', 'typography', [
            'headline_hex_color' => $headline_hex_color,
            'copy_hex_color'     => $copy_hex_color,
            'links_hex_color'    => $links_hex_color,
        ]);
    }

    /**
     * Button Meta Box
     */
    public function meta_box_buttons()
    {
        $button_text_hex_color = (get_post_meta(get_the_ID(), '_button_text_hex_color', true)) ? get_post_meta(get_the_ID(), '_button_text_hex_color', true) : $this->default_color;
        $button_color_hex_color = (get_post_meta(get_the_ID(), '_button_color_hex_color', true)) ? get_post_meta(get_the_ID(), '_button_color_hex_color', true) : $this->default_color;

        $this->include_partial('meta-box', 'buttons', [
            'button_text_hex_color'  => $button_text_hex_color,
            'button_color_hex_color' => $button_color_hex_color,
        ]);
    }

    /**
     * Document Meta Box
     */
    public function meta_box_document()
    {
        $body_background_hex_color = (get_post_meta(get_the_ID(), '_body_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_body_background_hex_color', true) : $this->default_color;
        $panel_background_hex_color = (get_post_meta(get_the_ID(), '_panel_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_panel_background_hex_color', true) : $this->default_color;

        $this->include_partial('meta-box', 'document', [
            'body_background_hex_color'  => $body_background_hex_color,
            'panel_background_hex_color' => $panel_background_hex_color,
        ]);
    }

    /**
     * Button Meta Box
     */
    public function meta_box_footer()
    {
        $footer_background_hex_color = (get_post_meta(get_the_ID(), '_footer_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_footer_background_hex_color', true) : $this->default_color;
        $footer_text_hex_color = (get_post_meta(get_the_ID(), '_footer_text_hex_color', true)) ? get_post_meta(get_the_ID(), '_footer_text_hex_color', true) : $this->default_color;

        $this->include_partial('meta-box', 'footer', [
            'footer_background_hex_color' => $footer_background_hex_color,
            'footer_text_hex_color'       => $footer_text_hex_color,
        ]);
    }

    /**
     * Button Meta Box
     */
    public function meta_box_theme()
    {
        $filtered_aggregators = $this->get_filtered_aggregators();
        $primary_hex_color = (get_post_meta(get_the_ID(), '_primary_hex_color', true)) ? get_post_meta(get_the_ID(), '_primary_hex_color', true) : $this->default_color;
        $headline_hex_color = (get_post_meta(get_the_ID(), '_headline_hex_color', true)) ? get_post_meta(get_the_ID(), '_headline_hex_color', true) : $this->default_color;
        $copy_hex_color = (get_post_meta(get_the_ID(), '_copy_hex_color', true)) ? get_post_meta(get_the_ID(), '_copy_hex_color', true) : $this->default_color;
        $links_hex_color = (get_post_meta(get_the_ID(), '_links_hex_color', true)) ? get_post_meta(get_the_ID(), '_links_hex_color', true) : $this->default_color;
        $body_background_hex_color = (get_post_meta(get_the_ID(), '_body_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_body_background_hex_color', true) : $this->default_color;
        $panel_background_hex_color = (get_post_meta(get_the_ID(), '_panel_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_panel_background_hex_color', true) : $this->default_color;
        $button_text_hex_color = (get_post_meta(get_the_ID(), '_button_text_hex_color', true)) ? get_post_meta(get_the_ID(), '_button_text_hex_color', true) : $this->default_color;
        $button_color_hex_color = (get_post_meta(get_the_ID(), '_button_color_hex_color', true)) ? get_post_meta(get_the_ID(), '_button_color_hex_color', true) : $this->default_color;
        $footer_background_hex_color = (get_post_meta(get_the_ID(), '_footer_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_footer_background_hex_color', true) : $this->default_color;
        $footer_text_hex_color = (get_post_meta(get_the_ID(), '_footer_text_hex_color', true)) ? get_post_meta(get_the_ID(), '_footer_text_hex_color', true) : $this->default_color;
        $info_bar_background_hex_color = (get_post_meta(get_the_ID(), '_info_bar_background_hex_color', true)) ? get_post_meta(get_the_ID(), '_info_bar_background_hex_color', true) : $this->default_color;
        $info_bar_text_hex_color = (get_post_meta(get_the_ID(), '_info_bar_text_hex_color', true)) ? get_post_meta(get_the_ID(), '_info_bar_text_hex_color', true) : $this->default_color;

        $this->include_partial('meta-box', 'theme', [
            'filtered_aggregators'          => $filtered_aggregators,
            'primary_hex_color'             => $primary_hex_color,
            'headline_hex_color'            => $headline_hex_color,
            'copy_hex_color'                => $copy_hex_color,
            'links_hex_color'               => $links_hex_color,
            'body_background_hex_color'     => $body_background_hex_color,
            'panel_background_hex_color'    => $panel_background_hex_color,
            'button_text_hex_color'         => $button_text_hex_color,
            'button_color_hex_color'        => $button_color_hex_color,
            'footer_background_hex_color'   => $footer_background_hex_color,
            'footer_text_hex_color'         => $footer_text_hex_color,
            'info_bar_background_hex_color' => $info_bar_background_hex_color,
            'info_bar_text_hex_color'       => $info_bar_text_hex_color,
        ]);
    }

    /**
     * Include a partial to use in our admin functions
     *
     * @param string $slug the first part of the filename
     * @param string $name the second part of the filename
     * @param array  $args
     */
    private function include_partial($slug = '', $name = '', $args = [])
    {
        extract($args);
        /** @noinspection PhpIncludeInspection - This is a dynamic file inclusion */
        include plugin_dir_path(__FILE__) . '/partials/' . $slug . (empty($name) ? '' : '-' . $name) . '.php';
    }

    /**
     * Return aggregators that have not yet been used in a theme
     *
     * @return string
     */
    private function get_filtered_aggregators()
    {
        global $wpdb;

        $used_aggregators = $wpdb->get_results("
            SELECT DISTINCT tt.term_taxonomy_id
            FROM {$wpdb->term_taxonomy} AS tt
            INNER JOIN {$wpdb->term_relationships} AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
            INNER JOIN {$wpdb->posts} AS p ON p.ID = r.object_id
            WHERE p.post_type = 'custom_theme'
            AND tt.taxonomy = 'aggregator'
        ", ARRAY_A);

        $aggregator_array = [];

        foreach ($used_aggregators as $aggregator) {
            $aggregator_array[] = $aggregator['term_taxonomy_id'];
        }

        return implode(',', $aggregator_array);
    }

    /**
     * Check that a service id is provided and that is matches the targeted theme post type
     */
    private function is_service()
    {
        if (isset($_GET['service_id'])) {
            if (get_post_type($_GET['service_id']) == (get_option('theme_services_post_type'))) {
                return true;
            }
        } elseif (strlen(get_post_meta(get_the_ID(), '_service_theme_post_id', true)) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get the link to the attached theme
     *
     * @return bool|string
     */
    private function get_theme_link()
    {
        if (strlen(get_post_meta(get_the_id(), '_custom_theme_post_id', true)) > 0) {
            return '/post.php?action=edit&post=' . get_post_meta(get_the_id(), '_custom_theme_post_id', true);
        }

        return false;
    }

    /**
     * Get the link to the service that the theme is attached to
     *
     * @param $service_post_id
     *
     * @return bool|string
     */
    private function get_service_link($service_post_id)
    {
        if ($service_post_id) {
            return admin_url() . 'post.php?action=edit&post=' . $service_post_id;
        }

        return false;
    }

    /**
     * @return mixed|null
     */
    private function get_service_post_id()
    {
        $service_post_id = null;

        if (strlen(get_post_meta(get_the_id(), '_service_theme_post_id', true)) > 0) {
            $service_post_id = get_post_meta(get_the_id(), '_service_theme_post_id', true);
        } elseif ($_GET['service_id']) {
            $service_post_id = $_GET['service_id'];
        }

        return $service_post_id;
    }

    /**
     * Redirect back to the post/service page when saving or updating a theme that is attached to a service
     */
    public function service_theme_redirect()
    {
        if (strlen(get_post_meta(get_the_id(), '_service_theme_post_id', true)) > 0) {
            $redirect_url = admin_url() . 'post.php?action=edit&post=' . get_post_meta(get_the_id(), '_service_theme_post_id', true);
            wp_redirect($redirect_url);
        }
    }

    /**
     *
     */
    public function remove_custom_theme_post_id()
    {
        if (get_post_type(get_the_id()) == 'custom_theme') {
            if (strlen(get_post_meta(get_the_id(), '_service_theme_post_id', true)) > 0) {
                delete_post_meta(get_post_meta(get_the_id(), '_service_theme_post_id', true), '_custom_theme_post_id');
            }
        }
    }

    /**
     * Remove the Billing Channels taxonomy from the Themes plugin menu
     */
    public function remove_aggregator_sub_menu_item()
    {
        remove_submenu_page('edit.php?post_type=custom_theme', 'edit-tags.php?taxonomy=aggregator&amp;post_type=custom_theme');
    }

    /**
     * @return array
     */
    private function get_templates()
    {
        global $wpdb;
        $templates_table = $wpdb->prefix . 'templates';
        $fields_table = $wpdb->prefix . 'fields';
        $template_fields_table = $wpdb->prefix . 'template_fields';

        $result = $wpdb->get_results(
            "  SELECT
                        $templates_table.name as 'template_name', 
                        $templates_table.slug as 'template_slug', 
                        $templates_table.template_key,
                        $fields_table.name as 'field_name',
                        $fields_table.slug as 'field_slug',
                        CONCAT($templates_table.`slug`, '_', $fields_table.`slug`) as 'postmeta_field'
                        FROM $templates_table
                        JOIN $template_fields_table ON $templates_table.id = $template_fields_table.template_id
                        JOIN $fields_table ON $template_fields_table.field_id = $fields_table.id"
        );

        $template_array = [];

        foreach ($result as $fields) {
            $template_array[$fields->template_name][] = (array)$fields;
        }

        return $template_array;
    }

    /**
     * Change the order of the Custom Theme submenu
     *
     * @param $menu_ord
     *
     * @return mixed
     */
    public function change_custom_theme_submenu_order($menu_ord)
    {
        global $submenu;

        $arr = [];
        $arr[] = $submenu['edit.php?post_type=custom_theme'][5];  // Note List
        $arr[] = $submenu['edit.php?post_type=custom_theme'][16];
        $arr[] = $submenu['edit.php?post_type=custom_theme'][17];
        $arr[] = $submenu['edit.php?post_type=custom_theme'][18];
        $arr[] = $submenu['edit.php?post_type=custom_theme'][10]; // Add New Note

        $submenu['edit.php?post_type=custom_theme'] = $arr;

        return $menu_ord;
    }
}
