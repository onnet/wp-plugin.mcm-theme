<p class="swatch-inline">
    <strong><?php _e('Headline'); ?></strong><br/>
    <label class="screen-reader-text" for="headline_hex_color"><?php _e('Headline'); ?></label>
</p>
<div>
    <input name="headline_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="headline_hex_color" value="<?php echo $headline_hex_color; ?>"/>
    <div id="swatch_headline" class="square" style="background-color: <?php echo $headline_hex_color; ?>"></div>
    <span id="headline_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>


<p class="swatch-inline">
    <strong><?php _e('Copy'); ?></strong><br/>
    <label class="screen-reader-text" for="copy_hex_color"><?php _e('Copy'); ?></label>
</p>
<div>
    <input name="copy_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="copy_hex_color" value="<?php echo $copy_hex_color; ?>"/>
    <div id="swatch_copy" class="square" style="background-color: <?php echo $copy_hex_color; ?>"></div>
    <span id="copy_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>


<p class="swatch-inline">
    <strong><?php _e('Links'); ?></strong><br/>
    <label class="screen-reader-text" for="links_hex_color"><?php _e(''); ?></label>
</p>
<div>
    <input name="links_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="links_hex_color" value="<?php echo $links_hex_color; ?>"/>
    <div id="swatch_links" class="square" style="background-color: <?php echo $links_hex_color; ?>"></div>
    <span id="links_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>
