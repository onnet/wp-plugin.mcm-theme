<p>Service Title: <?php echo $service_title ?></p>
<p>Service Post Id: <?php echo $service_post_id ?></p>
<label>
    <div>
        <input type="checkbox" name="hide_logo" id="hide_logo" <?php checked('1', get_post_meta(get_the_ID(), '_hide_logo', true), true); ?>/>
        <?php _e('Hide Logo in Footer'); ?>
    </div>
    <br>
    <div>
        <input type="checkbox" name="theme_active" id="theme_active" <?php checked('1', get_post_meta(get_the_ID(), '_theme_active', true), true); ?>/>
        <?php _e('Activate Theme'); ?>
    </div>
</label>
<br>
<a href="<?php
echo $service_link; ?>" class="button button-primary"><?php _e('View Service'); ?></a>