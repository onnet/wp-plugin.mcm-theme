<div class="info-panel">
    Variables available: <i><strong>{msisdn} {service_title} {service_cost} {partner_name} {support_number} {billing_cycle} {svc_name}</strong></i>
</div>

<div id='accordion'>
    <?php
    // TODO: move this logic to the admin class
    foreach ($templates as $template_name => $template_fields) {
        $template_key = ($template_fields[0]['template_key']) ? '(' . $template_fields[0]['template_key'] . ')' : '';
        echo "<h3>$template_name $template_key</h3><div>";

        foreach ($template_fields as $k => $v) {
            $field_name = $v['postmeta_field'];
            $postmeta_key = '_' . $field_name;
            $field_value = (array_key_exists($postmeta_key, $theme_postmeta)) ? $theme_postmeta[$postmeta_key][0] : '';

            echo "<p><strong>{$v['field_name']}</strong></p>";
            echo "<label class='screen-reader-text' for='{$field_name}'></label>";
            echo "<textarea rows='1' cols='40' name='{$field_name}' id='excerpt'>{$field_value}</textarea>";
        }

        echo '</div>';
    } ?>
</div>
