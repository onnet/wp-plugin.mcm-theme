<ul id="theme-template-tabs" class="category-tabs">
    <li class="tabs"><a href="#theme-primary"><?php _e('Primary Palette'); ?></a></li>
    <li class=""><a href="#theme-typography"><?php _e('Typography'); ?></a></li>
    <li class=""><a href="#theme-document"><?php _e('Document'); ?></a></li>
    <li class=""><a href="#theme-buttons"><?php _e('Buttons'); ?></a></li>
    <li class=""><a href="#theme-footer"><?php _e('Footer'); ?></a></li>
    <li class=""><a href="#theme-info-bar"><?php _e('Info Bar'); ?></a></li>
</ul>

<div id="theme-primary" class="tabs-panel" style="">
    <p class="swatch-inline">
        <strong><?php _e('Primary Hex'); ?></strong><br/>
        <label class="screen-reader-text" for="primary_hex_color"><?php _e('Primary Hex Color'); ?></label>
    </p>
    <div>
        <input name="primary_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="primary_hex_color" value="<?php echo $primary_hex_color; ?>"/>
        <div id="swatch_primary" class="square" style="background-color: <?php echo $primary_hex_color; ?>"></div>
        <span id="primary_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
    <input type="hidden" id="filtered_aggregators" value="<?php echo $filtered_aggregators; ?>">
    <br>
</div>

<div id="theme-typography" class="tabs-panel" style="display: none;">
    <p class="swatch-inline">
        <strong><?php _e('Headline'); ?></strong><br/>
        <label class="screen-reader-text" for="headline_hex_color"><?php _e('Headline'); ?></label>
    </p>
    <div>
        <input name="headline_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="headline_hex_color" value="<?php echo $headline_hex_color; ?>"/>
        <div id="swatch_headline" class="square" style="background-color: <?php echo $headline_hex_color; ?>"></div>
        <span id="headline_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>


    <p class="swatch-inline">
        <strong><?php _e('Copy'); ?></strong><br/>
        <label class="screen-reader-text" for="copy_hex_color"><?php _e('Copy'); ?></label>
    </p>
    <div>
        <input name="copy_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="copy_hex_color" value="<?php echo $copy_hex_color; ?>"/>
        <div id="swatch_copy" class="square" style="background-color: <?php echo $copy_hex_color; ?>"></div>
        <span id="copy_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>


    <p class="swatch-inline">
        <strong><?php _e('Links'); ?></strong><br/>
        <label class="screen-reader-text" for="links_hex_color"><?php _e(''); ?></label>
    </p>
    <div>
        <input name="links_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="links_hex_color" value="<?php echo $links_hex_color; ?>"/>
        <div id="swatch_links" class="square" style="background-color: <?php echo $links_hex_color; ?>"></div>
        <span id="links_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
    <br>
</div>

<div id="theme-document" class="tabs-panel" style="display: none;">
    <p class="swatch-inline">
        <strong><?php _e('Body Background'); ?></strong><br/>
        <label class="screen-reader-text" for="body_background_hex_color"><?php _e('Body Background'); ?></label>
    </p>
    <div>
        <input name="body_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="body_background_hex_color" value="<?php echo $body_background_hex_color; ?>"/>
        <div id="swatch_body_background" class="square" style="background-color: <?php echo $body_background_hex_color; ?>"></div>
        <span id="body_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>

    <p class="swatch-inline">
        <strong><?php _e('Panel Background'); ?></strong><br/>
        <label class="screen-reader-text" for="panel_background_hex_color"><?php _e('Panel Background'); ?></label>
    </p>
    <div>
        <input name="panel_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="panel_background_hex_color" value="<?php echo $panel_background_hex_color; ?>"/>
        <div id="swatch_panel_background" class="square" style="background-color: <?php echo $panel_background_hex_color; ?>"></div>
        <span id="panel_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
    <br>
    <label>
        <input type="checkbox" name="panel_box_shadow" id="panel_box_shadow" <?php checked('1', get_post_meta(get_the_ID(), '_panel_box_shadow', true), true); ?>/>
        <?php _e('Enable Panel Box Shadow'); ?>
    </label>
    <br>
    <br>
</div>

<div id="theme-buttons" class="tabs-panel" style="display: none;">
    <p class="swatch-inline">
        <strong><?php _e('Button Text'); ?></strong><br/>
        <label class="screen-reader-text" for="button_text_hex_color"><?php _e('Button Text'); ?></label>
    </p>
    <div>
        <input name="button_text_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="button_text_hex_color" value="<?php echo $button_text_hex_color; ?>"/>
        <div id="swatch_button_text" class="square" style="background-color: <?php echo $button_text_hex_color; ?>"></div>
        <span id="button_text_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>

    <p class="swatch-inline">
        <strong><?php _e('Button Color'); ?></strong><br/>
        <label class="screen-reader-text" for="button_color_hex_color"><?php _e('Button Color'); ?></label>
    </p>
    <div>
        <input name="button_color_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="button_color_hex_color" value="<?php echo $button_color_hex_color; ?>"/>
        <div id="swatch_button_color" class="square" style="background-color: <?php echo $button_color_hex_color; ?>"></div>
        <span id="button_color_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
    <br>
</div>

<div id="theme-footer" class="tabs-panel" style="display: none;">
    <p class="swatch-inline">
        <strong><?php _e('Footer Background Color'); ?></strong><br/>
        <label class="screen-reader-text" for="footer_background_hex_color"><?php _e('Footer Background Color'); ?></label>
    </p>
    <div>
        <input name="footer_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="footer_background_hex_color" value="<?php echo $footer_background_hex_color; ?>"/>
        <div id="swatch_footer_background" class="square" style="background-color: <?php echo $footer_background_hex_color; ?>"></div>
        <span id="footer_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>

    <p class="swatch-inline">
        <strong><?php _e('Footer Text Color'); ?></strong><br/>
        <label class="screen-reader-text" for="footer_text_hex_color"><?php _e('Footer Text Color'); ?></label>
    </p>
    <div>
        <input name="footer_text_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="footer_text_hex_color" value="<?php echo $footer_text_hex_color; ?>"/>
        <div id="swatch_footer_text" class="square" style="background-color: <?php echo $footer_text_hex_color; ?>"></div>
        <span id="footer_text_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
    <br>
</div>

<div id="theme-info-bar" class="tabs-panel" style="display: none;">
    <p class="swatch-inline">
        <strong><?php _e('Info Bar Background Color'); ?></strong><br/>
        <label class="screen-reader-text" for="info_bar_background_hex_color"><?php _e('Info Bar Background Color'); ?></label>
    </p>
    <div>
        <input name="info_bar_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="info_bar_background_hex_color" value="<?php echo $info_bar_background_hex_color; ?>"/>
        <div id="swatch_info_bar_background" class="square" style="background-color: <?php echo $info_bar_background_hex_color; ?>"></div>
        <span id="info_bar_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>

    <p class="swatch-inline">
        <strong><?php _e('Info Bar Text Color'); ?></strong><br/>
        <label class="screen-reader-text" for="info_bar_text_hex_color"><?php _e('Info Bar Text Color'); ?></label>
    </p>
    <div>
        <input name="info_bar_text_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="info_bar_text_hex_color" value="<?php echo $info_bar_text_hex_color; ?>"/>
        <div id="swatch_info_bar_text" class="square" style="background-color: <?php echo $info_bar_text_hex_color; ?>"></div>
        <span id="info_bar_text_hex_color_error" class="hex-color-error">Invalid hex value</span>
    </div>
</div>
