<p>
    <a href="<?php if ($theme_link) {
    echo add_query_arg('service_id', get_the_ID(), admin_url($theme_link)); ?>" class="button button-primary"><?php _e('Edit theme');
    } else {
        echo add_query_arg('service_id', get_the_ID(), admin_url('/post-new.php?post_type=custom_theme')); ?>" class="button button-primary"><?php _e('Add theme');
    } ?></a>
</p>