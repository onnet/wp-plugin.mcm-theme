<p class="swatch-inline">
    <strong><?php _e('Body Background'); ?></strong><br/>
    <label class="screen-reader-text" for="body_background_hex_color"><?php _e('Body Background'); ?></label>
</p>
<div>
    <input name="body_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="body_background_hex_color" value="<?php echo $body_background_hex_color; ?>"/>
    <div id="swatch_body_background" class="square" style="background-color: <?php echo $body_background_hex_color; ?>"></div>
    <span id="body_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>

<p class="swatch-inline">
    <strong><?php _e('Panel Background'); ?></strong><br/>
    <label class="screen-reader-text" for="panel_background_hex_color"><?php _e('Panel Background'); ?></label>
</p>
<div>
    <input name="panel_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="panel_background_hex_color" value="<?php echo $panel_background_hex_color; ?>"/>
    <div id="swatch_panel_background" class="square" style="background-color: <?php echo $panel_background_hex_color; ?>"></div>
    <span id="panel_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>
<br>
<label>
    <input type="checkbox" name="panel_box_shadow" id="panel_box_shadow" <?php checked('1', get_post_meta(get_the_ID(), '_panel_box_shadow', true), true); ?>/>
    <?php _e('Enable Panel Box Shadow'); ?>
</label>