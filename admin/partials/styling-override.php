<style media="all">
    .theme-links-color {
        color: <?php echo $theme_hexes['_links_hex_color']; ?>;
    }

    .mcm-row h1 {
        color: <?php echo $theme_hexes['_headline_hex_color']; ?>;
    }

    .mcm-row h2 {
        color: <?php echo $theme_hexes['_copy_hex_color']; ?>;
        margin: 2px 0;
    }

    .mcm-button-neg {
        color: <?php echo $theme_hexes['_headline_hex_color']; ?>;
    }

    .mcm-button {
        color: <?php echo $theme_hexes['_button_text_hex_color']; ?>;
        background-color: <?php echo $theme_hexes['_button_color_hex_color']; ?>;
    }

    .theme-primary-color {
        color: <?php echo $theme_hexes['_primary_hex_color']; ?>;
    }

    .mcm-panel {
        background-color: <?php echo $theme_hexes['_body_background_hex_color']; ?>;
    <?php if (!$theme_hexes['_panel_box_shadow']) {echo 'box-shadow: none';} ?>;
    }

    .rounded {
        background-color: <?php echo $theme_hexes['_panel_background_hex_color']; ?>;
    }

    .mcm-footer {
        color: <?php echo $theme_hexes['_footer_text_hex_color']; ?>;
        background-color: <?php echo $theme_hexes['_footer_background_hex_color']; ?>;
    }

    .mcm-checkbox {
        color: <?php echo $theme_hexes['_copy_hex_color']; ?>;
    }

    .mcm-checkbox a {
        color: <?php echo $theme_hexes['_links_hex_color']; ?>;
    }

    .theme-footer {
        color: <?php echo $theme_hexes['_footer_text_hex_color']; ?>;
        background-color: <?php echo $theme_hexes['_footer_background_hex_color']; ?>;
    }

    .mcm-info-bar {
        color: <?php echo $theme_hexes['_info_bar_text_hex_color']; ?>;
        background-color: <?php echo $theme_hexes['_info_bar_background_hex_color']; ?>;
    }

    <?php if (get_query_var('active_service_theme')) { ?>

    .mcm-button-neg {
        border-spacing: 8px;
    }

    .mcm-cta-unit {
        margin-bottom: 16px;
    }

    /* Service level theme styles */
    .theme-light-font-weight {
        font-weight: 300;
    }

    .theme-large-text {
        font-size: 21px;
        width: 100%;
        font-weight: bold;
    }
    <?php } ?>


</style>