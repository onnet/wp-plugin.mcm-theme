<p class="swatch-inline">
    <strong><?php _e('Footer Background Color'); ?></strong><br/>
    <label class="screen-reader-text" for="footer_background_hex_color"><?php _e('Footer Background Color'); ?></label>
</p>
<div>
    <input name="footer_background_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="footer_background_hex_color" value="<?php echo $footer_background_hex_color; ?>"/>
    <div id="swatch_footer_background" class="square" style="background-color: <?php echo $footer_background_hex_color; ?>"></div>
    <span id="footer_background_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>

<p class="swatch-inline">
    <strong><?php _e('Footer Text Color'); ?></strong><br/>
    <label class="screen-reader-text" for="footer_text_hex_color"><?php _e('Footer Text Color'); ?></label>
</p>
<div>
    <input name="footer_text_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="footer_text_hex_color" value="<?php echo $footer_text_hex_color; ?>"/>
    <div id="swatch_footer_text" class="square" style="background-color: <?php echo $footer_text_hex_color; ?>"></div>
    <span id="footer_text_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>