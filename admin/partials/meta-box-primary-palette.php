<p class="swatch-inline">
    <strong><?php _e('Primary Hex'); ?></strong><br/>
    <label class="screen-reader-text" for="primary_hex_color"><?php _e('Primary Hex Color'); ?></label>
</p>
<div>
    <input name="primary_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="primary_hex_color" value="<?php echo $primary_hex_color; ?>"/>
    <div id="swatch_primary" class="square" style="background-color: <?php echo $primary_hex_color; ?>"></div>
    <span id="primary_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>
<input type="hidden" id="filtered_aggregators" value="<?php echo $filtered_aggregators; ?>">
