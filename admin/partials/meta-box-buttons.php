<p class="swatch-inline">
    <strong><?php _e('Button Text'); ?></strong><br/>
    <label class="screen-reader-text" for="button_text_hex_color"><?php _e('Button Text'); ?></label>
</p>
<div>
    <input name="button_text_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="button_text_hex_color" value="<?php echo $button_text_hex_color; ?>"/>
    <div id="swatch_button_text" class="square" style="background-color: <?php echo $button_text_hex_color; ?>"></div>
    <span id="button_text_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>

<p class="swatch-inline">
    <strong><?php _e('Button Color'); ?></strong><br/>
    <label class="screen-reader-text" for="button_color_hex_color"><?php _e('Button Color'); ?></label>
</p>
<div>
    <input name="button_color_hex_color" class="hex-color" maxlength="7" align="top" type="text" size="15" id="button_color_hex_color" value="<?php echo $button_color_hex_color; ?>"/>
    <div id="swatch_button_color" class="square" style="background-color: <?php echo $button_color_hex_color; ?>"></div>
    <span id="button_color_hex_color_error" class="hex-color-error">Invalid hex value</span>
</div>