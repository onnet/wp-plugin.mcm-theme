<p><strong><?php _e('Banner'); ?></strong></p>
<p>
    <a title="<?php _e('Set banner'); ?>" href="javascript:;" id="set-service-theme-banner" data-target="service_theme_banner" data-remove="remove-service-theme-banner">
        <?php $banner_status = display_theme_banner_preview(get_post_meta(get_the_ID(), '_service_theme_banner_attachment_id', true), 'Set banner'); ?>
    </a>
</p>

<p style="<?php echo $banner_status ? "" : "display: none;"; ?>">
    <a title="<?php _e('Remove banner'); ?>" href="javascript:;" id="remove-service-theme-banner" data-target="set-service-theme-banner" data-target-text="<?php _e('Set banner'); ?>"><?php _e('Remove banner'); ?></a>
</p>

<div id="service_theme_banner_full_size_container" style="<?php echo $banner_status ? "" : "display: none;"; ?>">
    <input type="checkbox" name="service_theme_banner_full_size" id="service_theme_banner_full_size" <?php checked('1', get_post_meta(get_the_ID(), '_service_theme_banner_full_size', true), true); ?>/>
    <?php _e('Display Full Size'); ?>
</div>

<input name="service_theme_post_id" type="hidden" class="widefat" id="service_theme_post_id" value="<?php echo $service_post_id ?>"/>
<input name="service_theme_banner_url" type="hidden" class="widefat" id="service_theme_banner_url" value="<?php echo get_post_meta(get_the_ID(), '_service_theme_banner_url', true); ?>"/>
<input name="service_theme_banner_attachment_id" type="hidden" css="widefat" id="service_theme_banner_attachment_id" value="<?php echo get_post_meta(get_the_ID(), '_service_theme_banner_attachment_id', true); ?>"/>