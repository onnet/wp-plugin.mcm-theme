<div class="wrap" id="settings-<?php echo $this->plugin_name; ?>">
    <h2><?php echo __('Template Theming Configuration', $this->plugin_name); ?></h2>
    <form method="post" action="options.php">
        <?php
        // This prints out all hidden setting fields
        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);
        submit_button();
        ?>
    </form>
</div>