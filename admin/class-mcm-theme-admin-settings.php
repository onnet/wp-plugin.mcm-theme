<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://bitbucket.org/onnet
 * @since      1.0.0
 *
 * @package    theme
 * @subpackage theme/admin
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class Mcm_Theme_Admin_Settings
{
    /**
     * The ID of this plugin.
     *
     * @var string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @var string $version The current version of this plugin.
     */
    private $version;

    /**
     * Current settings
     *
     * @var array current settings
     */
    private $settings = [];

    /**
     * Settings options
     *
     * @var array List of settings
     */
    private $settings_options = [];

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     *
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version     The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->settings_options = [
            ['id' => 'theme_services_system', 'title' => __('System')],
        ];
        $this->settings = $this->get_settings();
    }

    /**
     * Add Options Page
     */
    public function add_options_page()
    {
        add_options_page(__('Template Theming'), __('Template Theming'), 'manage_options', 'options-' . $this->plugin_name, [$this, 'options_page']);
    }

    /**
     * Display options page
     */
    public function options_page()
    {
        include plugin_dir_path(__FILE__) . 'partials/settings.php';
    }

    /**
     * Add the fields for global service settings page
     */
    public function get_settings()
    {
        foreach ($this->settings_options as $setting) {
            $this->settings[$setting['id']] = get_option($setting['id']);
        }

        return $this->settings;
    }

    /**
     * Update option callbacks for all settings
     */
    public function update_option_hooks()
    {
        foreach ($this->settings_options as $setting) {
            add_action('add_option_' . $setting['id'], [$this, $setting['id'] . '_insert_callback'], 10, 2);
            add_action('update_option_' . $setting['id'], [$this, $setting['id'] . '_update_callback'], 10, 3);
        }
    }

    /**
     * Register all the global service settings fields
     */
    public function register_service_settings()
    {
        add_settings_section($this->plugin_name, __('Template Theming Settings'), [$this, 'settings_section_callback'], $this->plugin_name);
        foreach ($this->settings_options as $setting) {
            register_setting($this->plugin_name, $setting['id']);
            add_settings_field(
                $setting['id'],
                $setting['title'],
                [$this, $setting['id'] . '_input_callback'],
                $this->plugin_name,
                $this->plugin_name,
                [
                    'id'        => $setting['id'],
                    'label_for' => $setting['id'],
                    'title'     => $setting['title'],
                ]
            );
        }
    }

    /**
     * Unused but is a required, from WordPress, as a param in add_settings_section()
     *
     * @return bool
     */
    public function settings_section_callback($arg)
    {
        return false;
    }

    /**
     * @param $args
     */
    public function theme_services_system_input_callback($args)
    {
        include plugin_dir_path(__FILE__) . 'partials/setting-system.php';
    }

    /**
     * Insert option
     *
     * @param string $option_name
     * @param mixed  $value
     */
    public function theme_services_system_insert_callback($option_name, $value)
    {
        $this->theme_services_system_update_callback('', $value, $option_name);
    }

    /**
     * Update csc_services_system option
     *
     * @param mixed  $old_value
     * @param mixed  $value
     * @param string $option_name
     */
    public function theme_services_system_update_callback($old_value, $value, $option_name)
    {
        // This is the stock default 'Portal SDK'
        $options = [
            'theme_services_post_type'            => 'service',
            'theme_service_id_meta_key'           => 'service_id',
            'theme_service_thumbnail_id_meta_key' => '_thumbnail_id',
            'theme_service_title_meta_key'        => '_service_title',
        ];

        // Set the options for mcm
        if ('mcm' == $value) {
            $options = [
                'theme_services_post_type'  => 'post',
                'theme_service_id_meta_key' => 'club_id',
            ];

            $this->create_mcm_template_table();
        } else {
            $this->create_psdk_template_table();
        }

        // Update options
        foreach ($options as $option_name => $option_value) {
            update_option($option_name, $option_value);
        }
    }

    private function create_mcm_template_table()
    {
        deactivate_theme();
        activate_theme();
    }

    private function create_psdk_template_table()
    {
        deactivate_theme();
        activate_theme();
    }
}