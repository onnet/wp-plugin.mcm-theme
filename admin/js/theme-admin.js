(function ($) {
    "use strict";

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(window).load(function () {

        hideAggregatorTaxonomy();
        disableAggregators();
        hideFullSizeCheckbox();
        toggelTabs("#theme-template-tabs");
        $("#accordion").accordion();


        $(".hex-color").keyup(function () {
            var isHexColor = /^#[0-9A-F]{6}$/i.test($(this).val());
            if (isHexColor) {
                setSingleSwatch(this, $(this).val());
                $("#" + $(this).attr("id") + "_error").hide();
            } else {
                setSingleSwatch(this, "#ffffff");
                $("#" + $(this).attr("id") + "_error").show();

            }
        });

        $("a#set-service-theme-banner").on("click", function (e) {

            // Stop the anchor's default behavior
            e.preventDefault();

            // Display the media uploader
            renderMediaUploader($(this));
        });
        $("a#remove-service-theme-banner").on("click", function (e) {
            // Stop the anchor's default behavior
            e.preventDefault();

            // Add the set button text again
            var set_target = $("a#" + $(this).data("target"));

            // Add the set button text again
            $(set_target).empty().append($(this).data("target-text"));

            // Remove the information in the fields
            var data_target = $(set_target).data("target");
            $("#" + data_target + "_url").val("");
            $("#" + data_target + "_attachment_id").val("");

            // Hide remove button
            $(this).parent().hide();

            // Uncheck the 'Full Size' checkbox and then hide the option
            $("#" + data_target + "_full_size").prop('checked', false);
            $("#" + data_target + "_full_size_container").hide();
        });
    });

    // Set swatch colours
    function setSingleSwatch(el, backgroundColour) {
        $(el).next().css("background", backgroundColour);
    }

    // Disable aggregators that are being used in other themes
    function disableAggregators() {
        var usedAggregators = $("#filtered_aggregators").val();
        var usedAggregatorsArray = usedAggregators.split(",");

        $.each(usedAggregatorsArray, function (index, value) {
            if (!$("#" + "in-aggregator-" + value).is(":checked")) {
                $("#" + "in-aggregator-" + value).attr("disabled", true).attr("readonly", true).attr("checked", true);
                $("#" + "in-aggregator-" + value).closest("label").css("color", "grey").css("font-style", "italic");
            }
        });
    }

})(jQuery);

/**
 * Callback function for the 'click' event of the 'Set Footer Image'
 * anchor in its meta box.
 *
 * Displays the media uploader for selecting an image.
 */
function renderMediaUploader(target) {
    "use strict";

    var file_frame, image_data, media_title;

    media_title = jQuery(target).data("media-title");
    media_title = undefined === media_title ? jQuery(target).attr("title") : media_title;

    /**
     * If an instance of file_frame already exists, then we can open it
     * rather than creating a new instance.
     */
    if (undefined !== file_frame) {
        file_frame.open();
        return;
    }

    /**
     * If we're this far, then an instance does not exist, so we need to
     * create our own.
     *
     * Here, use the wp.media library to define the settings of the Media
     * Uploader.
     */
    file_frame = wp.media.frames.file_frame = wp.media({
        title: media_title,
        button: {
            text: jQuery(target).attr("title")
        },
        multiple: false  // Set to true to allow multiple files to be selected
    });

    /**
     * Setup an event handler for what to do when an image has been
     * selected.
     *
     * We need to make sure that the handler is attached
     * to the select event.
     */
    file_frame.on("select", function (e) {
        var attachment = file_frame.state().get("selection").first().toJSON();

        var img = "<img width=\"266\"  class=\"attachment-266x266 size-266x266\" alt=\"${title}\" src=\"${src}\" srcset=\"\" sizes=\"(max-width: 266px) 100vw, 266px\">";
        jQuery(target).empty().append(jQuery.tmpl(img, {
            title: attachment.title,
            src: attachment.sizes.full.url
        }));

        // Set the banner url & attachment id
        var data_target = jQuery(target).data("target");
        jQuery("#" + data_target + "_url").val(attachment.sizes.full.url);
        jQuery("#" + data_target + "_attachment_url").val(attachment.sizes.full.url);
        jQuery("#" + data_target + "_attachment_id").val(attachment.id);

        // Show remove button
        var data_remove = jQuery(target).data("remove");
        jQuery(target).closest("div").find(".howto").show();
        jQuery("#" + data_remove).parent().show();

        // Show the 'Display Full Size' checkbox
        jQuery("#" + data_target + "_full_size_container").show();
    });

    // Now display the actual file_frame
    file_frame.open();
}

/**
 * Hide taxonomies for service level themes
 */
function hideAggregatorTaxonomy() {
    if (typeof jQuery("#service_theme_post_id").val() !== "undefined") {
        if (jQuery("#service_theme_post_id").val().length > 0) {
            jQuery("#aggregatordiv").hide()
        }
    }
}

function hideFullSizeCheckbox() {
    if (typeof (jQuery('#service_theme_banner_attachment_id').val()) !== "undefined") {
        if (jQuery('#service_theme_banner_attachment_id').val().length > 0) {
            // jQuery("#service_theme_banner_full_size_container").show();
        }
    }
}

/**
 * Toggle Interstial Tabs
 */
function toggelTabs(container) {
    jQuery("a", container).on("click", function (e) {
        e.preventDefault();
        var t = jQuery(this).attr("href");
        jQuery(this).parent().addClass("tabs").siblings("li").removeClass("tabs");
        jQuery(container).siblings(".tabs-panel").hide();
        jQuery(t).show();
    });
}

(function ($) {
    toggelTabs("#theme-template-tabs");
})(jQuery);
