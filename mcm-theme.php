<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/onnet/wp-plugin.mcm-theme
 * @package           theme
 *
 * @wordpress-plugin
 * Plugin Name:       MCM Theme
 * Plugin URI:        https://bitbucket.org/onnet/wp-plugin.mcm-theme
 * Description:       Enables theme discovery
 * Version:           3.3.5
 * Author:            Hyve Mobile
 * Author URI:        https://bitbucket.org/onnet/wp-plugin.mcm-theme
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mcm-theme
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
defined('WPINC') or die();

// Define base plugin path
if (!defined('MCM_THEME_PATH')) {
    /**
     * Define Portal SDK Path
     */
    define('MCM_THEME_PATH', dirname(__FILE__));
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('theme_VERSION', '3.3.5');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mcm-theme-activator.php
 */
function activate_theme()
{
    if (get_option('theme_services_system') == 'portal-sdk') {
        require_once plugin_dir_path(__FILE__) . 'includes/class-mcm-theme-activator-psdk.php';
    } else {
        require_once plugin_dir_path(__FILE__) . 'includes/class-mcm-theme-activator.php';
    }

    theme_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mcm-theme-deactivator.php
 */
function deactivate_theme()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-mcm-theme-deactivator.php';
    theme_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_theme');
register_deactivation_hook(__FILE__, 'deactivate_theme');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-mcm-theme.php';

define('MCM_THEME_PLUGIN_PATH', plugin_dir_path(__FILE__));

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_theme()
{
    $plugin = new Mcm_Theme();
    $plugin->run();
}
run_theme();
