<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    theme
 * @subpackage theme/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    theme
 * @subpackage theme/public
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class Mcm_Theme_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Network prefixes
     */
    const
        DEFAULT_PREFIX = '072',
        DEFAULT_PREFIX_TELKOM = '081',
        DEFAULT_PREFIX_VODACOM = '072',
        DEFAULT_PREFIX_CELLC = '064',
        DEFAULT_PREFIX_MTN = '073',
        DEFAULT_PREFIX_MTN_SDP = '073',
        DEFAULT_PREFIX_MTN_DEP_HYVE = '073';

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of the plugin.
     * @param string $version     The version of this plugin.
     *
     * @since    1.0.0
     *
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Add club processing query vars
     *
     * @param array $query_vars
     *
     * @return array
     */
    function add_query_vars($query_vars)
    {
        return $query_vars;
    }

    /**
     * Change the display template file
     *
     * @param string $template_location
     *
     * @return string
     */
    public function display_template_file($template_location = '')
    {
        $plugin_theme_location = MCM_THEME_PLUGIN_PATH . 'templates/content/cross-sell-chains.php';
        if (file_exists($plugin_theme_location)) {
            return $plugin_theme_location;
        }

        return $template_location;
    }

    /**
     * @param $theme_hexes
     *
     * @return mixed
     */
    function mcm_set_theme_hexes($theme_hexes)
    {
        $theme_post_id = null;

        // Get the default theme first (required to get base template copy)
        $theme_object = get_page_by_title('default', OBJECT, 'custom_theme');

        if (!(is_object($theme_object))) {
            $theme_object = get_page_by_title('global', OBJECT, 'custom_theme');
        }

        // Check that theme object was returned
        if (is_object($theme_object)) {
            $theme_post_id = $theme_object->ID;
            set_query_var('theme_default_post_id', $theme_post_id);
        }

        if (get_query_var('network', false)) {
            $bc_slug = apply_filters('alternate_network_slug', get_query_var('network', false));

            $posts_array = get_posts(
                [
                    'posts_per_page' => -1,
                    'post_type'      => 'custom_theme',
                    'tax_query'      => [
                        [
                            'taxonomy' => 'aggregator',
                            'field'    => 'slug',
                            'terms'    => $bc_slug,
                        ],
                    ],
                ]
            );

            if (isset($posts_array[0])) {
                $theme_post_id = $posts_array[0]->ID;
                set_query_var('theme_aggregator_post_id', $theme_post_id);
            }
        }

        // Check if a service level theme is attached to this post. If so, overwrite the theme_id
        if (get_post_meta(get_post_meta(get_the_ID(), '_custom_theme_post_id', true), '_theme_active', true)) {
            $theme_post_id = get_post_meta(get_the_ID(), '_custom_theme_post_id', true);
            set_query_var('theme_service_post_id', $theme_post_id);
        }

        if ($theme_post_id) {
            // Get theme post_meta if theme post_id was found
            $theme_post_meta = ($theme_post_id) ? get_post_meta($theme_post_id) : [];

            foreach ($theme_hexes as $hex_key => $hex_value) {
                if (array_key_exists($hex_key, $theme_post_meta)) {
                    $theme_hexes[$hex_key] = $theme_post_meta[$hex_key][0];
                }
            }

            do_action('set_theme_query_vars');

            return $theme_hexes;
        }

        return false;
    }

    public function theme_styles()
    {
        $theme_hexes = $this->get_theme_hexes();

        if ($theme_hexes) {
            $this->include_partial('styling-override', '', ['theme_hexes' => $theme_hexes]);
        }
    }

    /**
     * Get theme hexes to display on the templates
     *
     * @return array
     */
    private function get_theme_hexes()
    {
        return apply_filters('mcm_set_theme_hexes', [
            '_headline_hex_color'            => '',
            '_copy_hex_color'                => '',
            '_links_hex_color'               => '',
            '_button_text_hex_color'         => '',
            '_button_color_hex_color'        => '',
            '_body_background_hex_color'     => '',
            '_panel_background_hex_color'    => '',
            '_primary_hex_color'             => '',
            '_footer_background_hex_color'   => '',
            '_footer_text_hex_color'         => '',
            '_info_bar_background_hex_color' => '',
            '_info_bar_text_hex_color'       => '',
            '_panel_box_shadow'              => 1,
        ]);
    }

    /**
     * @param int    $banner_id
     * @param string $return
     *
     * @return array|string
     */
    public function get_theme_banner($banner_id = 0, $return = 'url')
    {
        $attachment_key = 'featured_' . (is_feature_phone() ? 'feature' : 'mobile');

        if (get_post_meta(get_query_var('custom_theme_id'), '_service_theme_banner_attachment_id', true)) {
            $banner_id = get_post_meta(get_query_var('custom_theme_id'), '_service_theme_banner_attachment_id', true);
            if (get_post_meta(get_query_var('custom_theme_id'), '_service_theme_banner_full_size', true)) {
                $attachment_key = 'full';
            }
        } else {
            $banner_id = get_post_thumbnail_id(get_the_ID());
            if (get_post_meta(get_query_var('theme_default_post_id'), '_default_theme_banner_full_size', true)) {
                $attachment_key = 'full';
            }
        }

        // Detect image type that needs to be used
        $attachment_src = wp_get_attachment_image_src($banner_id, $attachment_key);

        // Build the image url
        $img_url = (!empty($attachment_src)) ? $attachment_src[0] : '';

        // Return the image url
        switch ($return) {
            case 'array':
                return [
                    'url'        => $attachment_src[0],
                    'width'      => $attachment_src[1],
                    'height'     => $attachment_src[2],
                    'size-label' => $attachment_key,
                ];
                break;

            default:
            case 'url':
                return $img_url;
                break;
        }
    }

    /**
     * @param int $banner_id
     */
    public function display_theme_banner($banner_id = 0)
    {
        // Defaults
        if ($banner_id == 0) {
            $banner_id = get_query_var('service_theme_banner_id');
        }

        // Get the service banner and display
        $theme_banner = apply_filters('get_theme_banner', $banner_id, 'array');
        echo sprintf('<!--suppress HtmlUnknownTarget --><img height="%2$d" src="%3$s" class="attachment-%4$s size-%4$s wp-post-image" alt="%5$s">',
            $theme_banner['width'],
            $theme_banner['height'],
            $theme_banner['url'],
            $theme_banner['size-label'],
            get_the_title()
        );
    }

    /**
     * Check if the service has a theme banner id attached to it and set the banner id as a query var
     */
    public function set_theme_banner_id()
    {
        if ($theme_id = get_post_meta(get_the_ID(), '_custom_theme_post_id', true)) {
            if (($banner_id = get_post_meta($theme_id, '_service_theme_banner_attachment_id', true)) && (get_post_meta($theme_id, '_theme_active', true))) {
                set_query_var('service_theme_banner_id', $banner_id);
            }
        }
    }

    /**
     * Set theme query var
     */
    public function set_theme_query_vars()
    {
        if ($theme_id = get_post_meta(get_the_ID(), '_custom_theme_post_id', true)) {
            if ((get_post_meta($theme_id, '_theme_active', true))) {
                set_query_var('active_service_theme', true);
            }

            if ((get_post_meta($theme_id, '_hide_logo', true))) {
                set_query_var('hide_logo_theme', true);
            }

            // Get the network logo attachment ID is has one set and is not been hidden on the theme
            if (get_query_var('network')) {
                if (!(get_post_meta($theme_id, '_hide_logo', true))) {
                    // Get the logo
                    $aggregator_term = apply_filters('get_aggregator_term', get_query_var('network'));
                    $term_taxonomy_id = isset($aggregator_term->term_taxonomy_id) ? $aggregator_term->term_taxonomy_id : false;
                    $attachment_id = get_metadata('term_taxonomy', $term_taxonomy_id, 'logo', true);

                    if ($attachment_id) {
                        set_query_var('logo_attachment_id', $attachment_id);
                    }
                }
            }

            set_query_var('custom_theme_id', $theme_id);
        }
    }

    /**
     * Include a partial to use in our public functions
     *
     * @param string $slug the first part of the filename
     * @param string $name the second part of the filename
     * @param array  $args
     */
    private function include_partial($slug = '', $name = '', $args = [])
    {
        extract($args);
        /** @noinspection PhpIncludeInspection - This is a dynamic file inclusion */
        include plugin_dir_path(__FILE__) . '../admin/partials/' . $slug . (empty($name) ? '' : '-' . $name) . '.php';
    }

    /**
     * Register the stylesheets for the admin area.
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/app.min.css', ['base'], '3.1.0');
    }

    public function template_copy()
    {
        $template_code = (get_option('theme_services_system') == 'portal-sdk') ? 'pid' : 'process_subscription';

        // Get the template key for the current template
        $template_key = (get_query_var($template_code, false)) ? (float)get_query_var($template_code, false) : null;

        global $wp_query;

        $theme_level_post_ids = [
            'theme_default_post_id'    => ((get_query_var('theme_default_post_id')) ? get_query_var('theme_default_post_id') : null),
            'theme_aggregator_post_id' => ((get_query_var('theme_aggregator_post_id')) ? get_query_var('theme_aggregator_post_id') : null),
            'theme_service_post_id'    => ((get_query_var('theme_service_post_id')) ? get_query_var('theme_service_post_id') : null),
        ];

        $fields = $this->get_template_fields($template_key);
        $template_copy = [];

        foreach ($fields as $field) {
            $template_copy[$field->postmeta_field] = '';
        }

        // Loop through the different
        foreach ($theme_level_post_ids as $theme_level_post_id) {
            foreach ($template_copy as $field_key => $field_copy) {
                if ($theme_level_post_id) {
                    $copy_postmeta = get_post_meta($theme_level_post_id, '_' . $field_key, true);
                    $template_copy[$field_key] = ($copy_postmeta) ? $copy_postmeta : $template_copy[$field_key];
                }
            }
        }

        foreach ($template_copy as $field_key => $field_value) {
            set_query_var($field_key, $this->parse_template_copy($field_value));
        }
    }

    /**
     * @param $template_key
     *
     * @return array|null|object
     */
    private function get_template_fields($template_key)
    {
        global $wpdb;
        $templates_table = $wpdb->prefix . 'templates';
        $fields_table = $wpdb->prefix . 'fields';
        $template_fields_table = $wpdb->prefix . 'template_fields';

        // Required because mysql is throwing an error when the variable is an empty string in the actual query
        $query_string = ($template_key) ? "= $template_key" : "= ''";

        $result = $wpdb->get_results(
            "  SELECT 
                    $templates_table.slug as 'template_slug', 
                    CONCAT($templates_table.slug, '_', $fields_table.slug) as 'postmeta_field'
                    FROM $templates_table
                    JOIN $template_fields_table ON $templates_table.id = $template_fields_table.template_id
                    JOIN $fields_table ON $template_fields_table.field_id = $fields_table.id
                    WHERE $templates_table.template_key $query_string"
        );

        return $result;
    }

    /**
     * Display the service footer
     */
    private function parse_template_copy($copy)
    {
        // Do some variable replacing
        $replacement_count = 0;

        if (get_option('theme_services_system') == 'portal-sdk') {
            $replacements = [
                'service_title'  => the_title('', '', false),
                'service_cost'   => psdk_price_point('<span>', '</span>', true),
                'partner_name'   => get_post_meta(get_the_ID(), '_waspa_name', true),
                'support_number' => get_post_meta(get_the_ID(), '_support_number', true),
                'msisdn'         => apply_filters('psdk_friendly_msisdn', get_query_var('msisdn')),
            ];
        } else {
            $replacements = [
                'service_title'  => get_post_meta(get_the_ID(), '_service_title', true),
                'service_cost'   => get_post_meta(get_the_ID(), 'charge_amount', true),
                'partner_name'   => get_post_meta(get_the_ID(), '_waspa_name', true),
                'support_number' => get_post_meta(get_the_ID(), '_support_number', true),
                'msisdn'         => mcm_get_friendly_msisdn(),
                'billing_cycle'  => get_query_var('subscription_billing_cycle'),
                'svc_name'       => get_the_title(),
            ];
        }

        // Split into Sentences
        $sentences = preg_split('/(?<=[.?!])\s+(?=)/i', $copy);

        // Start replacement
        foreach ($sentences as $sentence_key => $sentence_text) {
            foreach ($replacements as $search => $replace) {
                $replace = trim($replace);
                $sentence_text = str_replace([
                    '{' . $search . '}',
                    '###' . $search . '###',
                ], $replace, $sentence_text, $replacement_count);
            }

            // Set the sentence text
            $sentences[$sentence_key] = $sentence_text;
        }

        // Output the footer
        return (nl2br(implode(" ", $sentences)));
    }

    /**
     * Flow Template Part
     *
     * @param string $name
     */
    public function theme_flow_template_part($name = '')
    {
        // Defaults
        $templates = [];
        $base = 'theme-flow-templates/';
        $default = 'default';
        $name = '' !== $name ? $name : apply_filters('get_template_status_slug', mcm_get_current_process());
        set_query_var('template_slug', $name);

        // Platform Hosts
        $platform_host = get_query_var('platform_host');
        if ('' !== $platform_host) {
            $templates[] = "{$base}{$platform_host}-{$name}.php";
        }

        // Base default template
        // This is done last because it's the last template that needs to be search - (Fallback)
        $templates[] = "{$base}{$name}.php";

        locate_template($templates, true, false);
    }

    /**
     * @param $copy_part
     *
     * @return mixed
     */
    public function get_template_copy($copy_part)
    {
        $query_var_key = get_query_var('template_slug', true) . '_' . $copy_part;

        return get_query_var($query_var_key);
    }

    /**
     * Generate the placeholder for the wifi template. Required so that the network prefix is meaningful
     */
    public function get_service_network_placeholder()
    {
        $prefix = Mcm_Theme_Public::DEFAULT_PREFIX;

        if (get_option('theme_services_system') != 'portal-sdk') {
            $aggregators = wp_get_post_terms(get_the_ID(), 'aggregator');

            if (count($aggregators) === 1) {
                switch ($aggregators[0]->slug) {
                    case 'cellc':
                        $prefix = Mcm_Theme_Public::DEFAULT_PREFIX_CELLC;
                        break;
                    case 'telkom':
                        $prefix = Mcm_Theme_Public::DEFAULT_PREFIX_TELKOM;
                        break;
                    case 'mtn':
                        $prefix = Mcm_Theme_Public::DEFAULT_PREFIX_MTN;
                        break;
                    case 'mtn-sdp':
                        $prefix = Mcm_Theme_Public::DEFAULT_PREFIX_MTN;
                        break;
                }
            }
        }

        echo 'e.g. ' . $prefix . ' 555 5555';
    }
}
