<!doctype html>
<?php echo '<html lang="en">'; ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?php echo get_the_excerpt(); ?>">

    <title><?php the_title(); ?></title>
    <?php get_favicons(); ?>
    <?php wp_head(); ?>
    <?php do_action('mcm_custom_styles'); ?>
</head>
<body>
<div class="mcm-wrapper">
    <?php
    if ('post' === get_post_type()) {
        get_theme_flow_template_part();
    }
    ?>


<!--    --><?php
//if (have_posts()):
//        // Check if there is a theme available to apply
//        if (get_query_var('theme_default_post_id', true)) {
//            get_theme_flow_template_part();
//        }
//endif;
//?>

<?php do_action('ga_track_page_view'); ?>