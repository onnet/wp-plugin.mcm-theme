## 3.3.5

- [Hotfix] Added billing_cycle and svc_name variables for templates

## 3.3.4

- [MCM-873] Updated CSS to allow for a spinner on the ussd-success template

## 3.3.3

- [MCM-760] Allow network prefix to be dynamic for the wifi template

## 3.3.2

- [Hotfix] Fixed incorrect plugin version number to reflect correctly on portals

## 3.3.1

- [Hotfix] Fixed incorrect template slug in plugin activator

## 3.3.0

- Added PSDK templates

## 3.2.4

- [Hotfix] Use post feature banner when no custom theme banner is set

## 3.2.3

- [Hotfix] Updated css

## 3.2.2

- Added option to display full size images as default

## 3.2.1

- [Hotfix] Fixed javascript bug that was breaking the theme-admin.js file

## 3.2.0

- [MCM-743] Added pre confirmation template for internal doi

## 3.1.2

- Added forced error template (3.8) to plugin activate db queries

## 3.1.1

- [Hotfix] Updated mcm-img style height

## 3.0.6

- Updated plugin version
- Added similar service interstitial for wap-ussd
- Added notification bar

## 3.0.6

- Parse friendly msisdn for template copy

## 3.0.5

- Updated the stylesheet version

## 3.0.4

- Updated the stylesheet

## 3.0.3

- Added filter to change the location of the cross sell template file

## 3.0.2

- Added info bar to theme customization

## 3.0.1

- Updated activator to fix bug where the duplicate sub template wasn't populated

## 3.0.0

- Added dynamic template copy

## 2.1.1

- Added PSDK comparable DOI theme styles.

## 2.1

- Added option to display full size service theme banner

## 2.0.5

- Added a hide logo toggle for service level themes

## 2.0.4

- Added service level theming styles

## 2.0.3

## 2.0.2

## 2.0.1

- Updated plugin version

## 2.0

- Added service level theming
- Added PDSK compatibility

## 1.0.1

- Refactored theme logic
- Updated theme css
- Changed the post_type from theme to custom_theme

## 1.0

- Initial build of MCM Theme plugin
