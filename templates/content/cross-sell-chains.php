<?php if (!empty(get_query_var('_csc_relations', []))): ?>
    <div class="mcm-divider"></div>
    <h2 class="h1">Here’s some awesome content hand-picked just for you!</h2>

    <?php foreach (get_query_var('_csc_relations', []) as $key => $relation): ?>
        <?php if ($key % 2 == 0): ?>
            <div class="mcm-row spacing">
        <?php endif; ?>

        <div class="mcm-col-2 rounded">
            <a href="<?php echo apply_filters('mcm_parse_url', get_the_permalink($relation['post_id'])); ?>">
                <div class="mcm-img">
                    <?php echo apply_filters('get_image_html', $relation['attachment_id']); ?>
                    <div class="mcm-img-caption">
                        <span><?php echo get_post_meta($relation['post_id'], '_service_title', true); ?></span>
                    </div>
                </div>
            </a>
        </div>

        <?php if ($key % 2 != 0 || ($key + 1) === count(get_query_var('_csc_relations', []))): ?>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>
<?php endif; ?>


